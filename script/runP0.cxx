#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TSystem.h"
#include "TString.h"

#include "string.h"
#include <vector>

//#include "include/Background.h"

#include "/export/share/gauss/woodsn/hass_p0_script_trunk/macros/runSig.C"



void runP0(const char* mass, const char *inputdir )
{
  
  TString tmpmass = mass;
  tmpmass.ReplaceAll("NWA", "");
  tmpmass.ReplaceAll("LWA", "");

  /*
  string wname = "combined";
  string mname = "ModelConfig";
  string dname = "obsData";
  string xmass = tmpmass.Data();
  */

  gSystem->Load("liblvqq.so");

  string wname = "combined";
  //string wname = "combWS";
  string mname = "ModelConfig";
  string dname = "obsData";
  //string dname = "combData";
  string xmass = tmpmass.Data();

  //  string fname = Form("%s/hwwlvqq_combined_%s_model.root", inputdir, mass);

//  string fname = Form("/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/ws_combined/100ws_injec_nowsxs_nov20/%s/ws_HPLP_%s.root", inputdir, mass);
  string fname = Form("/export/share/gauss/woodsn/takuto_lvqq/StatToolsWVlvqq/ws_combined/ws_injec_syst_jan29/HVTWW/36fb/ws_HPLP_%s.root", mass);

  string folder = string(inputdir) + "_sig";

  cout << "file name : " << fname << endl;
  cout << "output folder : " << folder << endl; 

  runSig( fname.c_str(), //"workspaces/lnuqq_em01jet_370.root",
	  wname.c_str(), //"wwspace",
	  mname.c_str(), //"ModelConfig",
	  dname.c_str(), //"combData", 
	  "asimovData_1", 
          "conditionalGlobs_1",
	  "nominalGlobs",
	  xmass , 
	  folder
	  ) ;

}
