// C++
#include "string.h"
#include <vector>

// ROOT
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TSystem.h"

// Local
#include "macros/runAsymptoticsCLs.C"

using namespace std;


int runCLs( ){ return 0 ; }

int runCLs( int imode, int isys, int mass  ){
  
  double CLs = 0.95 ;

  //
  string pname(""), sname("") ;
  if (imode==500) { pname = "lnuqq_e0jet"    ;    }
  if (imode==501) { pname = "lnuqq_em01jet"  ;    }
  if (imode==502) { pname = "lnuqq_em2jet"   ;    }
  if (imode==503) { pname = "lnuqq_em012jet" ;    }
  //
  if (isys==0) sname = "_nosys";
  if (isys==1) sname = "";
  //
  string wname = "wwspace";
  string mname = "ModelConfig";
  string bname = "";
  string dname = "combData";
  string fname = Form("workspaces%s/%s_%3.0d.root", sname.c_str(), pname.c_str(), mass);
  string xmass = Form("%d", mass);
  string folder = pname+sname+"_cls" ; 
  

  cout << "file name : " << fname << endl;
  
  
  //main
  //void runAsymptoticsCLs(const char* infile,
  //			 const char* workspaceName,
  //			 const char* modelConfigName,
  //			 const char* dataName,
  //			 const char* asimovDataName,
  //			 string folder,
  //			 string mass,
  //			 double CL);
  



  runAsymptoticsCLs( fname.c_str(),  // input file name
		     wname.c_str(),  // workspace name
		     mname.c_str(),  // ModelConfig name
		     dname.c_str(),  // Data name
		     "asimovData",   // Asimove data name
		     folder,  // channel
		     xmass,  // mass  
		     CLs  
		    );
  
  return 0;

} 
