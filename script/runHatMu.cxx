#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TSystem.h"
#include "TString.h"

#include "string.h"
#include <vector>


#include "macros/runMuhat.C"



void runHatMu( int imode, const char* mass, const char *inputdir )
{
  
  string pname(""), sname("") ;
  if (imode==501) { pname = "ggF"  ;    }
  if (imode==502) { pname = "WBF"   ;    }
  if (imode==503) { pname = "ggFWBF" ;    }


  
  string wname = "combined";
  string mname = "ModelConfig";
  string dname = "obsData";
  string xmass = (TString(mass).ReplaceAll("NWA", "")).Data() ; 


  string fname = Form("%s/hwwlvqq_combined_%s_%s_model.root", inputdir, pname.c_str(), mass);



  string folder = string(inputdir) + "_" + pname + "_cls";

  cout << "file name : " << fname << endl;
  cout << "output folder : " << folder << endl; 
  
  runMuhat( fname, 
	    double(mass),  
	    folder,  
	    wname.c_str(),
	    mname.c_str(), 
	    dname.c_str()
	    ) ; 



}
