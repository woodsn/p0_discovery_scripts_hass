#!/bin/sh

for v in v11_ZZ
do
    #for im in 500 600 700 750 800 900 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000
    for im in 500 600 700 800 900 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000
    #for im in 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000
    do
	bsub -q 1nh -o ${PWD}/p0_${v}_${im}.out -J ${v}_${im}_p0 -u oabouzei "${PWD}/BSubSlave_p0 ${im} ${v} ${PWD}"
    done
done
