#include "macros/runInterp.C"

void runInterpLooper(int mass, string version)
{
  overriden = 1;
  runningLooper = 1;

  doee = 1;
  domm = 1;
  doem = 1;
  do0j = 1;
  do1j = 1;
  runInterp(mass, version);
  writeXML(mass, version);

  doee = 1;
  domm = 0;
  doem = 0;
  do0j = 1;
  do1j = 1;
  writeXML(mass, version+"_ee");

  doee = 0;
  domm = 1;
  doem = 0;
  do0j = 1;
  do1j = 1;
  writeXML(mass, version+"_mm");

  doee = 0;
  domm = 0;
  doem = 1;
  do0j = 1;
  do1j = 1;
  writeXML(mass, version+"_em");

  doee = 1;
  domm = 1;
  doem = 1;
  do0j = 1;
  do1j = 0;
  writeXML(mass, version+"_0j");

  doee = 1;
  domm = 1;
  doem = 1;
  do0j = 0;
  do1j = 1;
  writeXML(mass, version+"_1j");
}
