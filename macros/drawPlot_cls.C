
#include "macros/drawPlot.C"

#include "TLatex.h"
#include "TLine.h"
#include "TCanvas.h"

bool oldWay=0;
double maxLimit = 4;
double minLimit = 0.00005;
bool drawBands = 1;
bool doreread = 0;
string overlay="";

void drawPlot_cls2(string cardName);
void drawPlot_cls(string cardName, bool rereadAscii = 0, bool showZoom = 0, string overlayCard="")
{
  cardName += "_pv";
  computeFlags(cardName);
  dozoom = showZoom;
  doreread = rereadAscii;
  overlay=overlayCard;
  if (overlay != "") overlay += "_pv";

  if (dozoom)
  {
    overrideMass = 1;
    maxMass = 150;
  }

  //labelTxt="Private";
  ydiff_leg = 0.13;
  if (drawBands) ydiff_leg += 0.03;
  labelPosX = 0.2;
  if (dozoom) labelPosX = 0.2;
  labelPosY = 0.89;
  TCanvas* c1 = new TCanvas("c1","c1",1024,768);
  if (dogg)
  {
    lumi = "4.9";

    xmin_leg = 0.25;
    xdiff_leg = 0.22;
    ymax_leg = 0.36;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass = 110;
      maxMass = 150;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrow#gamma#gamma");
  }
  else if (dollll)
  {

    lumi = "4.9";

    xmin_leg = 0.62;
    xdiff_leg = 0.22;
    ymax_leg = 0.55;

    txtPosX = xmin_leg;
    txtPosY = 0.30;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=120;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowZZ#rightarrowllll");
  }
  else if (dollqq)
  {
    lumi = "2.05";

    xmin_leg = 0.2;
    xdiff_leg = 0.22;
    ymax_leg = 0.38;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.06;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=200;
      maxMass=600;
    }

    minLimit = 0.008;

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowZZ#rightarrowllqq");
  }
  else if (dollvv)
  {
    lumi = "2.05";

    xmin_leg = 0.25;
    xdiff_leg = 0.22;
    ymax_leg = 0.88;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=200;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowZZ#rightarrowll#nu#nu");
  }
  else if (dolvlv)
  {
    lumi = "4.7";
    if (cardName.find("_BK_") != string::npos) lumi = "2.3";
    else if (cardName.find("_LM_") != string::npos) lumi = "2.4";

    xmin_leg = 0.58;
    xdiff_leg = 0.22;
    ymax_leg = 0.55;

    txtPosX = 0.58;
    txtPosY = 0.30;

    minLimit = 0.0000005;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=110;
      maxMass=600;
    }

    if (cardName.find("_lpt_") != string::npos) 
    {
      maxMass = 190;
    }

    if (dozoom)
    {
      xmin_leg = 0.28;
      xdiff_leg = 0.22;
      ymax_leg = 0.55;

      txtPosX = 0.28;
      txtPosY = 0.30;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    if (cardName.find("_ee_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nue#nu");
    else if (cardName.find("_em_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nu#mu#nu");
    else if (cardName.find("_mm_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrow#mu#nu#mu#nu");
    else if (cardName.find("_0j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu+0j");
    else if (cardName.find("_1j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu+1j");
    else if (cardName.find("_2j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu+2j");
    else if (cardName.find("_BK_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu B-K");
    else if (cardName.find("_LM_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu L-M");
    else if (cardName.find("_ee0j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nue#nu+0j");
    else if (cardName.find("_em0j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nu#mu#nu+0j");
    else if (cardName.find("_mm0j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrow#mu#nu#mu#nu+0j");
    else if (cardName.find("_ee1j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nue#nu+1j");
    else if (cardName.find("_em1j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nu#mu#nu+1j");
    else if (cardName.find("_mm1j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrow#mu#nu#mu#nu+1j");
    else if (cardName.find("_ee2j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nue#nu+2j");
    else if (cardName.find("_em2j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowe#nu#mu#nu+2j");
    else if (cardName.find("_mm2j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrow#mu#nu#mu#nu+2j");
    else if (cardName.find("_01j_") != string::npos) t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu+0+1j");
    else if (cardName.find("_cuts_") != string::npos) 
    {
      t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu");
      t.DrawLatex(0.67, 0.2, "m_{T} Cut");
    }
    else if (cardName.find("_lpt_") != string::npos) 
    {
      t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu");
      t.DrawLatex(0.67, 0.2, "Low pT alone");
    }
    else if (cardName.find("_hpt_") != string::npos) 
    {
      t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu");
      t.DrawLatex(0.67, 0.2, "High pT alone");
    }
    else t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW^{(*)}#rightarrowl#nul#nu");
  }
  else if (dolvqq)
  {
    lumi = "1.04";

    xmin_leg = 0.2;
    xdiff_leg = 0.22;
    ymax_leg = 0.45;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=240;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW#rightarrowl#nuqq");
  }
  else if (dolh)
  {
    lumi = "1.063";

    xmin_leg = 0.19;
    xdiff_leg = 0.22;
    ymax_leg = 0.45;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=110;
      maxMass=150;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrow#tau_{l}#tau_{h}");
  }
  else if (doll)
  {
    lumi = "1.063";

    xmin_leg = 0.35;
    xdiff_leg = 0.22;
    ymax_leg = 0.4;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=120;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrow#tau_{l}#tau_{l}+1j");
  }
  else if (dowh)
  {
    lumi = "1.063";

    xmin_leg = 0.2;
    xdiff_leg = 0.22;
    ymax_leg = 0.45;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=120;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "WH, H#rightarrowbb");
  }
  else if (dozh)
  {
    lumi = "1.063";

    xmin_leg = 0.2;
    xdiff_leg = 0.22;
    ymax_leg = 0.45;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=120;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "ZH, H#rightarrowbb");
  }
  else if (dovh)
  {
    lumi = "1.063";

    xmin_leg = 0.2;
    xdiff_leg = 0.22;
    ymax_leg = 0.45;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=120;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "VH, H#rightarrowbb");
  }
  else if (dozz)
  {
    lumi = "4.7-4.9";

    xmin_leg = 0.32;
    xdiff_leg = 0.22;
    ymax_leg = 0.52;

    txtPosX = xmin_leg;
    txtPosY = 0.24;

    if (!dozoom)
    {
      txtPosX = 0.3;
      txtPosY = 0.74;
      xmin_leg = 0.6;
      ymax_leg = 0.87;
    }


    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=120;
      maxMass=600;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowZZ");
  }
  else if (doww)
  {
    lumi = "1.04-2.05";

    xmin_leg = 0.58;
    xdiff_leg = 0.22;
    ymax_leg = 0.55;

    txtPosX = 0.58;
    txtPosY = 0.30;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=110;
      maxMass=300;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrowWW#rightarrow");
  }
  else if (dott)
  {
    lumi = "1.063";

    xmin_leg = 0.19;
    xdiff_leg = 0.22;
    ymax_leg = 0.45;

    txtPosX = xmin_leg+0.3;
    txtPosY = ymax_leg-0.08;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=110;
      maxMass=150;
    }

    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.4, labelPosY, "H#rightarrow#tau#tau");
  }
  else if (docb)
  {
    lumi = "4.6-4.9";

    xmin_leg = 0.25;
    xdiff_leg = 0.22;
    ymax_leg = 0.86;

    txtPosX = 0.49;
    txtPosY = 0.72;

    markerSize = 0.8;

    if (!overrideMass)
    {
      minMass=110;
      maxMass=600;
    }

    minLimit = 0.0000005;

    if (dozoom)
    {
      xmin_leg = 0.2;
      xdiff_leg = 0.22;
      ymax_leg = 0.57;

      txtPosX = 0.196;
      txtPosY = 0.281;

      minLimit = 0.000005;
    }


    drawPlot_cls2(cardName);

    TLatex t;
    t.SetNDC();
    t.DrawLatex(labelPosX + 0.5, labelPosY, "2011 Data");
  }
  else
  {

  }

  string saveName = cardName+"_cls";
  if (dozoom) saveName += "_zoom";
  if (overlay != "") saveName+="_comp";
  save(saveName, "eps", c1);
  save(saveName, "pdf", c1);
}



void drawPlot_cls2(string cardName)
{
  cout << "Drawing plot: " << cardName << endl;

//see if file exists
  ifstream testFile(("ascii/"+cardName+".txt").c_str());
  if (testFile.fail() || doreread)
  {
    saveAscii(cardName);
  }

  fileHolder numbers;
  drawPlot("ascii/"+cardName+".txt", 6, numbers);

  int nrPoints = numbers.massPoints.size();
  if (nrPoints == 0)
  {
//maybe ascii needs to be rewritten
    saveAscii(cardName);
    drawPlot("ascii/"+cardName+".txt", 6, numbers);
  }
  nrPoints = numbers.massPoints.size();

  fileHolder olFile;
  double* olPoints;
  double* olObs;
  double* olExp;
  if (overlay != "")
  {
    if (doreread) saveAscii(overlay);
    drawPlot("ascii/"+overlay+".txt", 6, olFile);
    olPoints = getAry(olFile.massPoints);
    olObs = getAry(olFile.getCol(0));
    olExp = getAry(olFile.getCol(1));
  }

  double* massPoints = getAry(numbers.massPoints);
  double* obs = getAry(numbers.getCol(0));
  double* exp = getAry(numbers.getCol(1));
  double* p2s = getAry(numbers.getCol(2));
  double* p1s = getAry(numbers.getCol(3));
  double* n1s = getAry(numbers.getCol(4));
  double* n2s = getAry(numbers.getCol(5));
  for (int i=0;i<nrPoints;i++)
  {
    obs[i] = oldWay ? 1 - obs[i] : obs[i];
    exp[i] = oldWay ? 1 - exp[i] : exp[i];
    p2s[i] = p2s[i] - exp[i];
    p1s[i] = p1s[i] - exp[i];
    n1s[i] = exp[i] - n1s[i];
    n2s[i] = exp[i] - n2s[i];

    if (overlay != "")
    {
      olObs[i] = oldWay ? 1 - olObs[i] : olObs[i];
      olExp[i] = oldWay ? 1 - olExp[i] : olExp[i];
    }
  }

  int c_2s = kYellow;
  int c_1s = kGreen;



  string ytitle = "CLs";
  TGraphAsymmErrors* g_2s = makeGraphErr(ytitle, nrPoints, massPoints, exp, n2s, p2s);
  g_2s->SetMinimum(minLimit);
  g_2s->SetMaximum(maxLimit);
  g_2s->SetFillColor(c_2s);
  g_2s->GetYaxis()->SetTitleOffset(1.2);
  if (drawBands) g_2s->Draw("al3");

  TGraphAsymmErrors* g_1s = makeGraphErr(ytitle, nrPoints, massPoints, exp, n1s, p1s);
  g_1s->SetFillColor(c_1s);
  g_1s->SetMinimum(minLimit);
  g_1s->SetMaximum(maxLimit);
  if (drawBands) g_1s->Draw("l3");

  TGraph* g_obs = makeGraph(ytitle, nrPoints, massPoints, obs);
  g_obs->SetLineStyle(1);
  g_obs->SetLineWidth(2);
  g_obs->SetMinimum(minLimit);
  g_obs->SetMaximum(maxLimit);
  g_obs->SetMarkerSize(markerSize);
  g_obs->GetYaxis()->SetTitleOffset(1.2);
  if (drawBands) g_obs->Draw("lp");
  else g_obs->Draw("alp");

  TGraph* g_exp = makeGraph(ytitle, nrPoints, massPoints, exp);
  g_exp->SetLineStyle(2);
  g_exp->SetLineWidth(2);
  g_exp->Draw("l");






  TLegend* leg = makeLeg();
  leg->AddEntry(g_obs, "Obs.","lp");
  leg->AddEntry(g_exp, "Exp.","l");
  if (drawBands)
  {
    leg->AddEntry(g_1s, "#pm1 #sigma","f");
    leg->AddEntry(g_2s, "#pm2 #sigma","f");
  }

  if (overlay != "")
  {
    cout << "Drawing overlay" << endl;
    TGraph* g_ol_obs = makeGraph(ytitle, nrPoints, massPoints, olObs);
    g_ol_obs->SetLineColor(kRed);
    g_ol_obs->SetMarkerColor(kRed);
    g_ol_obs->SetLineStyle(1);
    g_ol_obs->SetLineWidth(2);
    g_ol_obs->SetMinimum(minLimit);
    g_ol_obs->SetMaximum(maxLimit);
    g_ol_obs->SetMarkerSize(markerSize);
    g_ol_obs->GetYaxis()->SetTitleOffset(1.2);
    g_ol_obs->Draw("lp");

    TGraph* g_ol_exp = makeGraph(ytitle, nrPoints, massPoints, olExp);
    g_ol_exp->SetLineColor(kRed);
    g_ol_exp->SetMarkerColor(kRed);
    g_ol_exp->SetLineStyle(2);
    g_ol_exp->SetLineWidth(2);
    g_ol_exp->Draw("l");

    leg->AddEntry(g_ol_obs, "Observed bounded","lp");
    leg->AddEntry(g_ol_exp, "Expected bounded","l");

  }

  leg->Draw();

  TLine l;
  l.SetLineWidth(2);
  l.SetLineStyle(2); 
  l.SetLineColor(kRed);

  double lposx_lo = g_obs->GetXaxis()->GetXmin();
  double lposx_hi = g_obs->GetXaxis()->GetXmax();
  //l.DrawLine(lposx_lo,0.1,lposx_hi,0.1);
  l.DrawLine(lposx_lo,0.05,lposx_hi,0.05);
  l.DrawLine(lposx_lo,0.01,lposx_hi,0.01);

  TLatex t;
  t.SetTextColor(kRed);
  double delta = (lposx_hi-lposx_lo)*0.02;
  //t.DrawLatex(lposx_hi+delta, 0.1-0.005, "90%");
  t.DrawLatex(lposx_hi+delta, 0.05-0.005, "95%");
  t.DrawLatex(lposx_hi+delta, 0.01, "99%");


  t.SetTextColor(kBlack);
  t.SetNDC();
  stringstream lumiLatex;
  lumiLatex << "#splitline{#int Ldt = " << lumi << " fb^{-1}}{     #sqrt{s} = 7 TeV}";
  //lumiLatex << "#int Ldt = " << lumi << " fb^{-1}";
  t.DrawLatex(txtPosX,txtPosY,lumiLatex.str().c_str());
  //t.DrawLatex(txtPosX-0.01,txtPosY-0.09,"#sqrt{s} = 7 TeV, 2011 Data");

  if (showLabel) ATLASLabel(labelPosX,labelPosY,labelTxt.c_str(),1);

  c1->SetLogy(dolog);
}

