/*
Author: Aaron Armbruster
Data:   2011-11-16

Description:

A once useful script for converting old input cards into histograms

*/

#include "TH1D.h"
#include "TFile.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

void convertInputs(double mass, string version, bool alt = false)
{
  stringstream massStr;
  massStr << mass;
  string smass = massStr.str();

  stringstream altStr;
  if (alt) altStr << "_alt";
  string salt = altStr.str();

  bool skip = ((mass > 200 && mass < 220 || (mass == 220 && alt)) ||  (mass > 165 && mass < 170 || (mass == 170 && alt)));

  vector<string> sampleNames;
  sampleNames.push_back("ggf"+smass);
  sampleNames.push_back("vbf"+smass);
  sampleNames.push_back("ttbar");
  sampleNames.push_back("st");
  sampleNames.push_back("ww");
  sampleNames.push_back("wzzz");
  sampleNames.push_back("zjets");
  sampleNames.push_back("wjets");
  sampleNames.push_back("data");
  int nrSamples = sampleNames.size();

  vector<string> channelNames;
  channelNames.push_back("ee");
  channelNames.push_back("em");
  channelNames.push_back("mm");
  int nrChannels = channelNames.size();

  vector<string> jetNames;
  jetNames.push_back("0j");
  jetNames.push_back("1j");
  int nrJets = jetNames.size();

  vector<string> regionNames;
  regionNames.push_back("signalLike");
  regionNames.push_back("mainControl");
  regionNames.push_back("topbox");
  int nrRegions = regionNames.size();

  map<string, string> chanMap;
  chanMap["ee"] = "ee";
  chanMap["em"] = "emu";
  chanMap["mm"] = "mumu";

  map<string, TFile*> files;

  stringstream tmpFileName;
  tmpFileName << "rev/" << version << "/hists/"+smass+salt << "/Nominal/Normal/data.root";
  TFile tmpFile(tmpFileName.str().c_str());
  TH1D* tmpHist = (TH1D*)tmpFile.Get((channelNames[0]+"_"+regionNames[0]+"_"+jetNames[0]).c_str());
  int tmpBins = tmpHist->GetNbinsX();
  double xmin = tmpHist->GetXaxis()->GetXmin();
  double xmax = tmpHist->GetXaxis()->GetXmax();

  for (int isam=0;isam<nrSamples;isam++)
  {
    string fileName = "rev/"+version+"/normHists/"+smass+salt+"/Nominal/Normal/";
    system(("mkdir -vp " + fileName).c_str());
    fileName += sampleNames[isam]+".root";

    if (sampleNames[isam] == "wjets" && skip) continue;
    files[sampleNames[isam]] = new TFile(fileName.c_str(),"recreate");
  }

  for (int ichan=0;ichan<nrChannels;ichan++)
  {
    for (int ijet=0;ijet<nrJets;ijet++)
    {
      for (int ireg=0;ireg<nrRegions;ireg++)
      {
	string name = chanMap[channelNames[ichan]]+"_"+regionNames[ireg]+"_"+jetNames[ijet];
	string name2 = channelNames[ichan]+"_"+regionNames[ireg]+"_"+jetNames[ijet];

	if (name.find("topbox") != string::npos && name.find("0j") != string::npos) continue;

	cout << "Processing file: " << name << endl;

	ifstream inFile(("inputs/"+name).c_str());
	if (inFile.fail())
	{
	  cout << "ERROR::Couldn't open file: " << name << endl;
	}

	bool first = true;
	map<string, double> vals;
	int thisMass = 0;
	while (thisMass != mass)
	{
	  inFile >> thisMass;
	  //cout << "Considering mass: " << thisMass << endl;
	  for (int isam=0;isam<nrSamples;isam++)
	  {
	    if (sampleNames[isam] == "st") continue;
	    inFile >> vals[sampleNames[isam]];
	  }

	  if (thisMass == mass && first && alt) 
	  {
	    thisMass = 0;
	    first = false;
	  }
	}

	vals["st"] = pow(10., -9);

	for (int isam=0;isam<nrSamples;isam++)
	{
	  if (sampleNames[isam] == "wjets" && skip) continue;
	  files[sampleNames[isam]]->cd();
	  TH1D* hist = new TH1D(name2.c_str(),name2.c_str(),tmpBins,xmin,xmax);
	  hist->SetBinContent(1, vals[sampleNames[isam]]);
	}
      }
    }
  }

  for (int isam=0;isam<nrSamples;isam++)
  {
    if (sampleNames[isam] == "wjets" && skip) continue;
    files[sampleNames[isam]]->Write();
    files[sampleNames[isam]]->Close();
  }

  cout << "Finished converting inputs" << endl;
  //system(("cp rev/test5/normHists/"+smass+"/norms.txt rev/"+version+"/normHists/"+smass+"/norms.txt").c_str());
}
