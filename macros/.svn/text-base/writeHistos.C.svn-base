#ifndef WRITEHISTOS
#define WRITEHISTOS


/*
Author: Aaron Armbruster
Data:   2011-11-16

Description:

Write out all histograms to root files from input trees.

1) load rates into global objects from the trees
2) prepare mT mapping
3) fill histos

*/



#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"

#include "macros/setup.C"
#include "macros/fileHolder.C"

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <set>
#include <map>
#include <utility>
#include <fstream>

using namespace std;


void getRates(string sampleName, string folder, double mass, bool alt);


//0 = nothing
//1 = map to emu_signalLike_0j range
//2 = map to flat background
int mappingMode = 0;
bool doSingleBinCR = true;

void writeHistos(double mass = 0, string version = "test", bool alt = false)
{
  setup(mass, alt);

//setup initial variables, prepare list of sample names

  double scale = -1.0;//20.0;

  stringstream massStr;
  massStr << mass;
  string smass = massStr.str();

  stringstream altStr;
  if (alt) altStr << "_alt";
  string salt = altStr.str();

//   int nrBasePoints = 39;
//   int* baseMassPoints = new int[nrBasePoints];
//   int thisMass = 110;
//   int step = 5;
//   for (int i=0;i<nrBasePoints;i++)
//   {
//     baseMassPoints[i] = thisMass;
//     if (thisMass >= 200) step = 20;
//     thisMass += step;
//   }

//   int nrBasePoints = 30;
//   double* baseMassPoints = new double[nrBasePoints];
//   double thisMass = 110;
//   double step = 5;
//   for (int i=0;i<nrBasePoints;i++)
//   {
//     baseMassPoints[i] = thisMass;
//     //if (thisMass >= 200) step = 20;
//     if (thisMass >= 200 && thisMass < 300) step = 20;
//     else if (thisMass == 300 || thisMass == 440 || thisMass == 540) step = 60;
//     else if (thisMass == 360 || thisMass == 400 || thisMass == 500) step = 40;

//     thisMass += step;
//   }

  int nrBasePoints = 38;
  double* baseMassPoints = new double[nrBasePoints];
  double thisMass = 110;
  double step = 5;
  for (int i=0;i<nrBasePoints;i++)
  {
    if (thisMass == 480)
    {
      thisMass += 20;
      i--;
      continue;
    }
    baseMassPoints[i] = thisMass;
    if (thisMass >= 200) step = 20;

    thisMass += step;
  }

  int nrPoints = 1;
  double* massPoints = new double[nrPoints];
  massPoints[0] = mass;

 
  int nrSys = fileNames->size();

  vector<string> sampleNames;
  sampleNames.push_back("st");
  sampleNames.push_back("ttbar");
  sampleNames.push_back("ww");
  sampleNames.push_back("wzzz");
  if (doWgs) sampleNames.push_back("wgs");
  if (doWg) sampleNames.push_back("wg");
  sampleNames.push_back("zjets");
  for (int imass=0;imass<nrPoints;imass++)
  {
    bool found = false;
    for (int ibase=0;ibase<nrBasePoints;ibase++)
    {
      if (baseMassPoints[ibase] == massPoints[imass]) 
      {
	found = true;
	break;
      }
    }

// see if we have MC for this point. If not, wait and interpolate at a later stage
    if (!found) continue; 
    stringstream ggfName;
    ggfName << "ggf" << massPoints[imass];

    stringstream vbfName;
    vbfName << "vbf" << massPoints[imass];

    stringstream whName;
    whName << "wh" << massPoints[imass];

    stringstream zhName;
    zhName << "zh" << massPoints[imass];

    if (!doFermi) sampleNames.push_back(ggfName.str());
    sampleNames.push_back(vbfName.str());
    if (mass <= 300 && !doConf) 
    {
      sampleNames.push_back(whName.str());
      sampleNames.push_back(zhName.str());
    }
  }
  int nrSamples = sampleNames.size();





//load rates into global variables

  int nrReg = regions->size();
  Region* r_norm = &(*regions)[0]; // for normalizing mt to maximum sr
  int nrChans = r_norm->channels.size();


  if (doWjets)
  {
//wjets first
    getRates("wjets", "Nominal/Normal", mass, alt);
    getRates("wjets", "Fake_Rate/SysFakeUp", mass, alt);
    getRates("wjets", "Fake_Rate/SysFakeDown", mass, alt);
  }

//then everything else
  vector<string> folderNames;
  for (int isys=0;isys<nrSys;isys++)
  {
    set<string> folders;
    folders.insert((*fileNames)[isys].fileUp);
    folders.insert((*fileNames)[isys].fileDown);

    if ((*fileNames)[isys].folder == "Nominal")
    {
      getRates("data",(*fileNames)[isys].folder+"/Normal", mass, alt);
    }

    for (set<string>::iterator folder=folders.begin();folder!=folders.end();folder++)
    {
      stringstream command;
      
      if (!doMVA){
	string base = "output";
	if (doConf) base += "_conf";
	command << "mkdir -vp " << base+"/"+(*fileNames)[isys].folder+"/"+*folder;
      } else {
	command << "mkdir -vp " << basedir+"/H"+smass+"/"+(*fileNames)[isys].folder+"/"+*folder;
      }
      system(command.str().c_str());

      folderNames.push_back((*fileNames)[isys].folder+"/"+*folder);

      for (int isam=0;isam<nrSamples;isam++)
      {
	getRates(sampleNames[isam], (*fileNames)[isys].folder+"/"+*folder, mass, alt);
      }
    }
  }
  folderNames.push_back("Fake_Rate/SysFakeUp");
  folderNames.push_back("Fake_Rate/SysFakeDown");
  int nrFolders = folderNames.size();

  if (doWjets) sampleNames.push_back("wjets");
  sampleNames.push_back("data");
  nrSamples = sampleNames.size();


//Prepare mT mapping. Find mT points such that nominal total background is uniform for each channel
  map<string, set<double> > map_reg_boundaries;
  if (mappingMode == 2)
  {
    for (int ireg=0;ireg<nrReg;ireg++)
    {
      Region* r = &(*regions)[ireg];

      int nrChannels = r->channels.size();
      for (int ichan=0;ichan<nrChannels;ichan++)
      {
	Channel* c = &r->channels[ichan];
	if (c->jetName == "0j" && r->name.find("topbox") != string::npos) continue;
	if (c->jetName == "2j" && r->name.find("mainControl") != string::npos) continue;
	if (c->jetName == "2j" && r->name.find("lowPt") != string::npos) continue;

	int theseBins = nrBins_0j;
	if (r->name.find("lowPt") != string::npos) theseBins = nrBins_lowPt;
	else if (c->nj == 1)
	{
	  theseBins = nrBins_1j;
	}
	if (doMVA)
	{
	  theseBins=nrBins0j_MVA;
	  if (c->nj == 1) theseBins = nrBins1j_MVA;
	}



//find total background
	for (map<string, map<string, multiset<pair<double, double> > > >::iterator map_map_itr = c->sys_sample_rates.begin();
	     map_map_itr!=c->sys_sample_rates.end();map_map_itr++)
	{
	  if (map_map_itr->first.find("Nominal") == string::npos) continue;

	  string name = c->name + "_" + r->name + "_" + c->jetName;

	  cout << "On Channel: " << name << endl;
	  double tot = 0;
	  multiset<pair<double, double> > all_rates;
	  for (map<string, multiset<pair<double, double> > >::iterator map_itr = map_map_itr->second.begin();
	       map_itr != map_map_itr->second.end();map_itr++)
	  {
	    if (map_itr->first.find("ggf") != string::npos || map_itr->first.find("vbf") != string::npos || map_itr->first.find("wh") != string::npos || map_itr->first.find("zh") != string::npos || map_itr->first.find("data") != string::npos) continue; // sum bg only
	    for (multiset<pair<double, double> >::iterator set_itr = map_itr->second.begin();set_itr!=map_itr->second.end();set_itr++)
	    {
	      tot += set_itr->second;
	      all_rates.insert(*set_itr);
	    }
	  }

//find the rate we want per bin
	  double nrPerBin = tot/theseBins;
	  cout << "Total: " << tot << ", Exp per bin: " << nrPerBin << endl;
	  double sum_tot = 0;
	  tot = 0;
	  for (multiset<pair<double, double> >::iterator set_itr = all_rates.begin();set_itr!=all_rates.end();set_itr++)
	  {
	    sum_tot += set_itr->second;
	    tot += set_itr->second;
	    if (tot > nrPerBin && int(map_reg_boundaries[name].size()) < theseBins && set_itr->second >= 0 /* fu*%! */ && tot < nrPerBin*theseBins) // and find the mT points that satisfy that
	    {
	      cout << "tot = " << tot << ", inserting boundary: " << set_itr->first << endl;
	      map_reg_boundaries[name].insert(set_itr->first);
	      tot = 0;
	    }
	  }
	  cout << "sum_tot = " << sum_tot << endl;

	  cout << "Found the following boundaries for hist: " << name << endl;
	  for (set<double>::iterator itr=map_reg_boundaries[name].begin();itr!=map_reg_boundaries[name].end();itr++)
	  {
	    cout << "  -> " << *itr << endl;
	  }
	  cout << endl;
	  break;
	}
      }
    }
  }


  system(("mkdir -vp rev/"+version+"/hists/"+smass+salt).c_str());
  ofstream bdFile(("rev/"+version+"/hists/"+smass+salt+"/boundaries.txt").c_str());
  for (map<string, set<double> >::iterator itr=map_reg_boundaries.begin();itr!=map_reg_boundaries.end();itr++)
  {
    bdFile << itr->first;

    set<double> bds = itr->second;
    for (set<double>::iterator itr2=bds.begin();itr2!=bds.end();itr2++)
    {
      bdFile << " " << *itr2;
    }
    bdFile << "\n";
  }
  bdFile.close();


  double minVal_norm = *r_norm->all_rates.begin(); // used in alternate mapping scheme
  double maxVal_norm = *r_norm->all_rates.rbegin(); // (mapping to emu_signalLike_0j range)
  if (mappingMode == 2)
  {
    minVal_norm = 0;
    maxVal_norm = 1;
  }

//big nested loop to write all histograms
//   map<string, multiset<pair<double, double> >* > ggf_rates;
//   map<string, multiset<pair<double, double> >* > vbf_rates;
  for (int isys=0;isys<nrFolders;isys++)
  {
    string folder = smass+salt+"/"+folderNames[isys];
    cout << "Writing folder: " << folder << endl;
    for (int isam=0;isam<nrSamples;isam++)
    {
      string sampleName = sampleNames[isam];
      if (sampleName != "wjets" && folder.find("Fake_Rate") != string::npos) continue;

      cout << " -->Sample = " << sampleName << endl;
      system(("mkdir -vp rev/"+version+"/hists/"+folder).c_str());
      TFile* outFile = new TFile(("rev/"+version+"/hists/"+folder+"/"+sampleName+".root").c_str(),"recreate");
      for (int ireg=0;ireg<nrReg;ireg++)
      {
	Region* r = &(*regions)[ireg];
	cout << "   |-->Region = " << r->name << endl;


	double minVal = r->all_rates.size() == 0 ? 0 : *r->all_rates.begin();
	double maxVal = r->all_rates.size() == 0 ? 10e9 : *r->all_rates.rbegin();

	//cout << "done computing max/min" << endl;

	if (maxVal <= minVal)
	{
	  cout << "ERROR::Couldn't map mT distribution for region: " << r->name << endl;
	  exit(1);
	}

	//cout << "computing linear mapping " << endl;
//for mode 1, use linear mT' = mT * sf + offset
	double sf = mappingMode != 0 ? (maxVal_norm - minVal_norm)/(maxVal-minVal) : 1;
	double offset = mappingMode != 0 ? maxVal_norm - sf*maxVal : 0;

	//cout << "entering channel loop" << endl;
	for (int ichan=0;ichan<nrChans;ichan++)
	{
	  Channel* c = &r->channels[ichan];
	  //cout << "channel addy = " << c << endl;
	  if (c->jetName == "0j" && r->name.find("topbox") != string::npos) continue;
	  if (c->jetName == "2j" && r->name.find("mainControl") != string::npos) continue;
	  if (c->jetName == "2j" && r->name.find("lowPt") != string::npos) continue;

	  int theseBins = nrBins_0j;
	  if (r->name.find("lowPt") != string::npos) theseBins = nrBins_lowPt;
	  else if (c->nj == 1)
	  {
	    theseBins = nrBins_1j;
	  }
	  if (doMVA)
	  {
	    theseBins=nrBins0j_MVA;
	    if (c->nj == 1) theseBins = nrBins1j_MVA;
	  }

	  multiset<pair<double, double> >* rates = &c->sys_sample_rates[folder][sampleName];
	  

	  stringstream histName;
	  histName << c->name << "_" << r->name << "_" << c->jetName;

	  if (mappingMode == 0)
	  {
	    if (!doMVA)
	    {
	      minVal_norm = *r->all_rates.begin();
	      maxVal_norm = *r->all_rates.rbegin();
	    } 
	    else 
	    {
	      minVal_norm = -1;
	      maxVal_norm = 1;
	    }
	  }
	  TH1D* hist = new TH1D(histName.str().c_str(), histName.str().c_str(), theseBins, minVal_norm, maxVal_norm);
	  hist->Sumw2();


//main loop over multiset of mT and event weight, fill hists
	  set<double> boundaries = map_reg_boundaries[histName.str()];
	  set<double>::iterator bd = boundaries.begin();
	  int bin = 1;
	  double integral = 0;
	  for (multiset<pair<double, double> >::iterator itr = rates->begin();itr != rates->end();itr++)
	  {
	    if (mappingMode == 0 || mappingMode == 1)
	    {
	      hist->Fill(sf*itr->first + offset, itr->second);
	    }
	    else
	    {
	      while (itr->first > *bd && bd != boundaries.end()) // see if we need to move to the next bin
	      {
		bd++;
		bin++;
		//double bound = bd == boundaries.end() ? 10e9 : *bd;
	      }
	      double center = hist->GetBinCenter(bin); // just fill at the center of the desired bin
	      if (folder.find("Nominal") != string::npos && sampleNames[isam] == "data" && c->name == "mm" && c->jetName == "1j" && r->name == "signalLikeLM") 
	      {
		cout << "bin = " << bin << ", theseBins = " << theseBins << endl;
		cout << "center = " << center << ", weight = " << itr->second << endl;
	      }
	      hist->Fill(center, itr->second);
	    }
	    integral += itr->second;
	  }

	  if (scale > 0 && hist->Integral() > 0) // used for some interpolation tests
	  {
	    hist->Scale(scale/hist->Integral());
	  }

	  cout << "      |-->Channel = " << c->name+c->jetName << ", total = " << integral << endl;

	  if (doSingleBinCR)
	  {
	    if (r->name.find("mainControl") != string::npos || r->name.find("topbox") != string::npos)
	    {
	      hist->Rebin(hist->GetNbinsX());
	    }
	  }

	  if (r->name.find("signalLike") != string::npos && c->jetName == "2j")
	  {
	    if (doSingleBin2j)
	    {
	      hist->Rebin(hist->GetNbinsX());
	      if (sampleName == "zjets")
	      {
		double frac = 1.0;
		if (splitPeriods)
		{
		  if (r->name.find("BK") != string::npos)
		  {
		    frac = 2.1/4.7;
		  }
		  else
		  {
		    frac = 2.6/4.7;
		  }
		}

		if (mode == 0) // mT cut
		{
		  if (!useHighMass && !useHighMass2)
		  {
		    if (c->name == "ee")
		    {
		      hist->SetBinContent(1, 0.01*frac);
		    }
		    else if (c->name == "mm")
		    {
		      hist->SetBinContent(1, 0.03*frac);
		    }
		  }
		}
		else if (mode == 1)
		{
		  if (c->name == "ee")
		  {
		    hist->SetBinContent(1, 0.09*frac);
		  }
		  else if (c->name == "mm")
		  {
		    hist->SetBinContent(1, 0.18*frac);
		  }
		}
	      }
	    }
	  }



	  if (sampleName != "data") // if we have zero entries in a bin, just set value to very something small to avoid problems later
	  {
	    for (int ibin=0;ibin<=theseBins;ibin++)
	    {
	      if (hist->GetBinContent(ibin+1) <= 0) hist->SetBinContent(ibin+1, pow(10, -18));
	    }
	  }
	}
      }

      outFile->Write();
      outFile->Close();
    }
  }

//write the raw signal mt values to a new tree to be used for interpolation
  

}


//read in rates from trees
void getRates(string sampleName, string folder, double mass, bool alt)
{
  stringstream massStr;
  massStr << mass;
  string smass = massStr.str();

  stringstream altStr;
  if (alt) altStr << "_alt";
  string salt = altStr.str();

  cout << "Preparing rates: " << folder << " :: " << sampleName << endl;

  stringstream inFileName;
  string base = "output";
  if (doConf) base += "_conf";
  if (!doMVA)
  {
    inFileName << base+"/"+folder+"/"+sampleName;
  }
  else
  {
    inFileName << basedir+"/H"+smass+"/"+folder+"/"+sampleName;
  }
  if (sampleName == "wjets")
  {
    if (wjMode == 1)
    {
      inFileName << "_Z";
    }
    else if (wjMode == 2)
    {
      inFileName << "_Comb";
    }
  }
  inFileName << ".root";

  TFile* file = new TFile(inFileName.str().c_str());
  TTree* tree;

  if (!doMVA) 
  {
   tree = (TTree*)file->Get("Output");
  } 
  else 
  {
   tree = (TTree*)file->Get("MVATree");
  }

  float l0pt, l1pt, mll, mt, metrel, ptll, dphi;
  unsigned int urun;
  int nj, ne, nm, run, nbt, islowpt, centralJetVeto;
  float MVA_Mll, MVA_lepPt0, MVA_MT, MVA_METRel, MVA_Ptll, MVA_DPhill, MVA_Mjj, MVA_jetEta0, MVA_jetEta1; //this is needed because struct Variable uses doubles, but except for EventWeight and mva_weight, MVATree stores variables as float
  if (!doMVA)
  {
    tree->SetBranchAddress("w",&vars_ptr->w);
    tree->SetBranchAddress("l1pt",&l1pt);
    if (splitem) tree->SetBranchAddress("l0pt",&l0pt);
    tree->SetBranchAddress("mll",&mll);
    tree->SetBranchAddress("mt",&mt);
    tree->SetBranchAddress("metrel",&metrel);
    tree->SetBranchAddress("ptll",&ptll);
    tree->SetBranchAddress("dphi",&dphi);
    if (bt20GeV)
    {
      tree->SetBranchAddress("nbt20",&nbt);
    }
    else
    {
      tree->SetBranchAddress("nbt25",&nbt);
    }
    tree->SetBranchAddress("nj",&nj);
    tree->SetBranchAddress("ne",&ne);
    tree->SetBranchAddress("nm",&nm);
    tree->SetBranchAddress("run",&urun);
    tree->SetBranchAddress("islowpt",&islowpt);
  } 
  else 
  {
    tree->SetBranchAddress("EventWeight",&vars_ptr->w);
    tree->SetBranchAddress("lepPt0",&MVA_lepPt0);
    tree->SetBranchAddress("Mll",&MVA_Mll);
    tree->SetBranchAddress("MT",&MVA_MT);
    tree->SetBranchAddress("METRel",&MVA_METRel);
    tree->SetBranchAddress("Ptll",&MVA_Ptll);
    tree->SetBranchAddress("DPhill",&MVA_DPhill);
    tree->SetBranchAddress("mva_weight",&vars_ptr->mva_weight);
    tree->SetBranchAddress("nbt",&nbt);
    tree->SetBranchAddress("m_jet_n",&nj);
    tree->SetBranchAddress("m_el_n",&ne);
    tree->SetBranchAddress("m_mu_n",&nm);
    tree->SetBranchAddress("RunNumber",&run);
    tree->SetBranchAddress("Mjj",&MVA_Mjj);
    tree->SetBranchAddress("jetEta0",&MVA_jetEta0);
    tree->SetBranchAddress("jetEta1",&MVA_jetEta1);
    tree->SetBranchAddress("centralJetVeto",&centralJetVeto);
  }

  fileHolder fermi;
  double fermiScale = 1;
  if (doFermi)
  {
    fermi.setNrCols(3);
    fermi.readInFile("config/"+string(doConf?"conf/":"")+"fermi.txt");
    fermiScale = 1./fermi.getRateByMass(mass, 2);
  }


  folder = smass + salt + "/" + folder;
  int nrEntries = tree->GetEntries();
  int nrReg = regions->size();
  for (int entry=0;entry<nrEntries;entry++)
  {
    //if (entry % 1000 == 0 || entry == nrEntries - 1) cout << "->On entry " << entry+1 << " / " << nrEntries << endl; 
    tree->GetEntry(entry);

    if (!doMVA) 
    {
      vars_ptr->l1pt = l1pt;
      //vars_ptr->l2pt = l2pt;
      vars_ptr->mll = mll;
      vars_ptr->mt = mt;
      vars_ptr->metrel = metrel;
      vars_ptr->ptll = ptll;
      vars_ptr->dphi = dphi;
      vars_ptr->run = urun;
      vars_ptr->nbt = nbt;
      vars_ptr->islowpt = islowpt;
    } 
    else 
    {
      vars_ptr->run = run;
      vars_ptr->nbt = nbt;
      vars_ptr->mll = MVA_Mll/1000.0;
      vars_ptr->l1pt = MVA_lepPt0/1000.0;
      vars_ptr->mt = MVA_MT/1000.0;
      vars_ptr->metrel = MVA_METRel/1000.0;
      vars_ptr->ptll = MVA_Ptll/1000.0;
      vars_ptr->dphi = MVA_DPhill;
      vars_ptr->Mjj = MVA_Mjj/1000.0;
      vars_ptr->etaProdJJ = MVA_jetEta0*MVA_jetEta1;
      vars_ptr->DeltaEtaJJ = fabs(MVA_jetEta0-MVA_jetEta1);
      vars_ptr->cjv = centralJetVeto;
    }



    if (nj > 2) nj = 2;

    for (int ireg=0;ireg<nrReg;ireg++)
    {
      Region* r = &(*regions)[ireg];
      int nrChans = r->channels.size();
      
      for (int ichan=0;ichan<nrChans;ichan++)
      {
	Channel* c = &r->channels[ichan];
	if (c->ne != ne) continue;
	if (c->nm != nm) continue; // make channel selections
	if (c->nj != nj) continue;
	if (splitem)
	{
	  if (c->name == "em" && l0pt < l1pt) continue;
	  if (c->name == "me" && l0pt >= l1pt) continue;
	}


	//if (sampleName.find("ggf") != string::npos || sampleName.find("vbf") != string::npos) vars_ptr->w *= 1.5;
	bool passed = true;
	int nrCuts = r->cuts.size();
	for (int icut=0;icut<nrCuts;icut++) // apply pre-specified cuts
	{
	  if (!(passed = passed && r->cuts[icut].passed(ne, nm, nj))) break;
	}

	if (!passed) continue;
	if ((ne == 2 || nm == 2) && vars_ptr->metrel < metrelCut) continue;
	//if ((ne == 1 && nm == 1) && vars_ptr->metrel < 35) continue;
	if (nj == 0)
	{
	  if ((ne == 2 || nm == 2) && vars_ptr->ptll < ptllCut) continue;
	  if ((ne == 1 && nm == 1) && vars_ptr->ptll < ptllCut_em) continue;
	}
	if (sampleName == "zjets" && !useZabcd)
	{
	  if (doMVA)
	  {
            if (ne == 2 && nj == 0) vars_ptr->w *= 1.003; //ok
            if (nm == 2 && nj == 0) vars_ptr->w *= 1.057; //ok
            if (ne == 2 && nj == 1) vars_ptr->w *= 1.052; //1.060;
            if (nm == 2 && nj == 1) vars_ptr->w *= 0.919; //0.916;
	  }
	}

	if ((sampleName == "ttbar" || sampleName == "st") && nj == 0)
	{
	  if (!doMVA)
	  {
	    if (r->name.find("lowPt") != string::npos)
	    {
	      vars_ptr->w *= 1.212;
	    }
	    else
	    {
	      if (doConf)
	      {
		vars_ptr->w *= 1.056;
	      }
	      else
	      {
		vars_ptr->w *= 1.038;
	      }
	    }
	  }
          else 
	  {
            vars_ptr->w *= 1.056; // it was 1.060 until thursday 10.02.2012
          }
	}

	if (doFermi)
	{
	  if (sampleName.find("vbf") != string::npos || sampleName.find("wh") != string::npos || sampleName.find("zh") != string::npos)
	  {
	    vars_ptr->w *= fermiScale;
	  }
	}

	if (sampleName == "wjets") vars_ptr->w *= wjetsScale;

	//if (sampleName.find("ggf") != string::npos || sampleName.find("vbf") != string::npos) vars_ptr->w *= 1.5;


//load rates into this unnecessarily nested c++ object
	c->sys_sample_rates[folder][sampleName].insert(make_pair(*output_var, vars_ptr->w));
	r->all_rates.insert(*output_var);
      }
    }
  }
  //cout << "->Done with loop" << endl;

//rescale Z to the data-driven rate
  if (sampleName == "zjets" && useZabcd)
  {
    map<string, double> zjets_norms;
    if (splitPeriods)
    {
      vector<string> suffices;
      suffices.push_back("bk");
      suffices.push_back("lm");
      int nrSuf = suffices.size();

      for (int is=0;is<nrSuf;is++)
      {
	stringstream fileName;
	fileName << "config/"+string(doConf?"conf/":"")+"zjets_norms";
	if (mass <= massBoundary) fileName << "_lowm";
	else fileName << "_highm";
	fileName << "_" << suffices[is] << ".txt";

	ifstream zjets_norms_file(fileName.str().c_str());
	if (zjets_norms_file.bad() || zjets_norms_file.fail())
	{
	  cout << "ERROR::Couldn't open zjets norms file: " << fileName.str() << endl;
	  exit(1);
	}

	while (!zjets_norms_file.eof())
	{
	  string term;
	  zjets_norms_file >> term;
	  if (zjets_norms_file.eof()) break;
	  double val;
	  zjets_norms_file >> val;

	  zjets_norms[term] = val;
	}
	zjets_norms_file.close();
      }
    }
    else
    {
      stringstream fileName;
      fileName << "config/"+string(doConf?"conf/":"")+"zjets_norms";
      if (mass <= massBoundary) fileName << "_lowm";
      else fileName << "_highm";
      fileName << ".txt";

      ifstream zjets_norms_file(fileName.str().c_str());
      if (zjets_norms_file.bad() || zjets_norms_file.fail())
      {
	cout << "ERROR::Couldn't open zjets norms file: " << fileName.str() << endl;
	exit(1);
      }

      while (!zjets_norms_file.eof())
      {
	string term;
	zjets_norms_file >> term;
	if (zjets_norms_file.eof()) break;
	double val;
	zjets_norms_file >> val;

	zjets_norms[term] = val;
      }
      zjets_norms_file.close();
    }





//read in lowpt norms
    stringstream fileName;
    fileName << "config/"+string(doConf?"conf/":"")+"zjets_lowpt_norms.txt";

    ifstream zjets_norms_file(fileName.str().c_str());
    if (zjets_norms_file.bad() || zjets_norms_file.fail())
    {
      cout << "ERROR::Couldn't open zjets norms file: " << fileName.str() << endl;
      exit(1);
    }

    while (!zjets_norms_file.eof())
    {
      string term;
      zjets_norms_file >> term;
      if (zjets_norms_file.eof()) break;
      double val;
      zjets_norms_file >> val;

      zjets_norms[term] = val;
    }
    zjets_norms_file.close();




    for (int ireg=0;ireg<nrReg;ireg++)
    {
      Region* r = &(*regions)[ireg];
      int nrChans = r->channels.size();
      
      for (int ichan=0;ichan<nrChans;ichan++)
      {
	Channel* c = &r->channels[ichan];
	
	double norm = 0;
	multiset<pair<double, double> >* rates = &c->sys_sample_rates[folder][sampleName];
	for (multiset<pair<double, double> >::iterator itr=rates->begin();itr!=rates->end();itr++)
	{
	  norm += itr->second;
	}

	string channelName = c->name+"_"+r->name+"_"+c->jetName;

	if (zjets_norms.find(channelName) == zjets_norms.end())
	{
	  cout << "Didn't find data-driven normalization for channel " << channelName << endl;
	  continue;
	}

	double dd_norm = zjets_norms[channelName];

	if (norm < 0)
	{
	  cout << "ERROR::Scaling Z to be more negative!" << endl;
	  exit(1);
	}

	if (norm == 0)
	{
	  if (dd_norm > 0)
	  {
	    cout << "ERROR::Data-driven normalization non-zero, but no shape info available for channel " << channelName << endl;
	    //exit(1);
	  }

	  continue;
	}



	double scaleF = dd_norm;///norm;

	multiset<pair<double, double> > new_rates;
	for (multiset<pair<double, double> >::iterator itr=rates->begin();itr!=rates->end();itr++)
	{
	  new_rates.insert(make_pair(itr->first, itr->second*scaleF));
	}

	*rates = new_rates;

      }

    }
  }


  file->Close();
}


#endif
