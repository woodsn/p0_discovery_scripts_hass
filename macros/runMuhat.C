#include "TFile.h"
#include "TH1D.h"


#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooNLLVar.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "TStopwatch.h"
#include "RooSimultaneous.h"

#include "RooMinimizer.h"
#include "Math/MinimizerOptions.h"
//#include "Minuit2/Minuit2Minimizer.h"

#include "macros/makeData.C"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>

using namespace std;
using namespace RooFit;
using namespace RooStats;

int isatboundary=0;
double interp(double x, double x1, double x2, double y1, double y2);
void getError(int direction, RooNLLVar* nll, RooRealVar* firstPOI, double& err);
void minimize(RooNLLVar* nll, RooRealVar* firstPOI = NULL);
double runMuhat(string inFileName,
		double mass = 130,
		string outFolder = "test",
		string wsName = "combined",
		string mcName = "ModelConfig",
		string dataName = "obsData",
		string conditionalSnapshot = "",
		string nominalSnapshot = "")
{
  TStopwatch timer;
  timer.Start();

//check inputs
  TFile f(inFileName.c_str());
  RooWorkspace* ws = (RooWorkspace*)f.Get(wsName.c_str());
  if (!ws)
  {
    cout << "ERROR::Workspace: " << wsName << " doesn't exist!" << endl;
    return 0;
  }
  ModelConfig* mc = (ModelConfig*)ws->obj(mcName.c_str());
  if (!mc)
  {
    cout << "ERROR::ModelConfig: " << mcName << " doesn't exist!" << endl;
    return 0;
  }
  RooDataSet* data = (RooDataSet*)ws->data(dataName.c_str());
  if (!data)
  {
    cout << "ERROR::Dataset: " << dataName << " doesn't exist!" << endl;
    return 0;
  }
  RooRealVar* firstPOI = (RooRealVar*)mc->GetParametersOfInterest()->first();

  RooSimultaneous* simPdf = (RooSimultaneous*)mc->GetPdf();
  double min_mu;
  data = makeData(data, simPdf, mc->GetObservables(), firstPOI, mass, min_mu);
  //return;
  cout << "Found min mu = " << min_mu << endl;
  //firstPOI->setMin(min_mu);

  int strategy = 0;
  //RooNLLVar::SetIgnoreZeroEntries(1);
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
  ROOT::Math::MinimizerOptions::SetDefaultStrategy(strategy);
  ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(1);
  //RooMinuit::SetMaxIterations(10000);
  //RooMinimizer::SetMaxFunctionCalls(10000);



  RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);

  if (conditionalSnapshot != "") ws->loadSnapshot(conditionalSnapshot.c_str());


  //pdf->fitTo(*data, Hesse(0), Minos(1), PrintLevel(0));
  bool errIsComputed = 0;
  double errup, errdown;
  firstPOI->setVal(-1);
  firstPOI->setRange(-10, 20);
  firstPOI->setConstant(0);

  bool oldWay = false;
  if (oldWay)
  {
    RooAbsPdf* pdf = mc->GetPdf();
    RooNLLVar* fNll = (RooNLLVar*)pdf->createNLL(*data, RooFit::CloneData(kFALSE), Constrain(*mc->GetNuisanceParameters()));

    ws->loadSnapshot("ucmles");

    int strat = ROOT::Math::MinimizerOptions::DefaultStrategy();
    RooMinimizer minim(*fNll);
    minim.setStrategy(strat);
    minim.setPrintLevel(ROOT::Math::MinimizerOptions::DefaultPrintLevel());
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());


    if (status != 0 && status != 1 && strat < 2)
    {
      strat++;
      cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << endl;
      minim.setStrategy(strat);
      status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    }

    if (status != 0 && status != 1 && strat < 2)
    {
      strat++;
      cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << endl;
      minim.setStrategy(strat);
      status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    }

    if (nominalSnapshot != "") ws->loadSnapshot(nominalSnapshot.c_str());
  }
  else
  {
    bool useMinuit2 = 1;
    if (useMinuit2)
    {
      ws->loadSnapshot("ucmles");

      //firstPOI->setMin(0);
      RooNLLVar* nll = (RooNLLVar*)mc->GetPdf()->createNLL(*data);

      minimize(nll, firstPOI);//mc->GetPdf()->fitTo(*data,Save(),Strategy(strategy),PrintLevel(0));

      double minNll = nll->getVal();

      firstPOI->Print();
      //cout << "mu error hi "<<firstPOI->getErrorHi()<<endl;
      double firstmuhat = firstPOI->getVal();
      double firsterror = firstPOI->getError();

      // set mu at low value, evaluate nll to see if low value looks trust worthy
      firstPOI->setVal(firstmuhat-0.1*firsterror);
      nll->setEvalErrorLoggingMode(RooAbsReal::CountErrors);
      //cout << "Creating profile" << endl;
      //RooAbsReal* profile =   nll->createProfile(*mc->GetParametersOfInterest());
      double nllAtmuLo = nll->getVal()-minNll;
      cout << "nll = "<< nllAtmuLo<<endl;
      cout << "nll errors= "<< nll->numEvalErrors()<<endl;
      //  double pllAtmuLo = profile->getVal();
      //  cout << "pll = "<< pllAtmuLo<<endl;

      // set mu back to best fit
      firstPOI->setVal(firstmuhat);

      // if problems, set min to best fit
      if(nllAtmuLo>10 || nll->numEvalErrors()>0){
	//firstPOI->setMin(firstmuhat);
	isatboundary=1;
	cout << "is at boundary" << endl;
      }

      // get minos errors either way

      double mu_val = firstPOI->getVal();

      //RooArgSet nuisAndPoi(*mc->GetNuisanceParameters(), *firstPOI);
      ws->saveSnapshot("tmp_snapshot", *mc->GetPdf()->getParameters(data));

      getError(-1, nll, firstPOI, errdown);

      ws->loadSnapshot("tmp_snapshot");
      getError(1, nll, firstPOI, errup);
//       if (isatboundary)
//       {
// 	errdown = 0;
//       }
//       else
//       {
// 	getError(-1, nll, firstPOI, errdown);
//       }
      cout << "Found error = " << errup << ", " << errdown << endl;
      firstPOI->setConstant(0);

      firstPOI->setVal(mu_val);

      errIsComputed = 1;
      //mc->GetPdf()->fitTo(*data,Minos(*mc->GetParametersOfInterest()),Strategy(strategy),PrintLevel(0));

      //minimize(nll, 1, mc->GetParametersOfInterest());



    }
    else
    {



      ws->loadSnapshot("ucmles");
      firstPOI->setConstant(0);
      firstPOI->setMin(-10);
      RooNLLVar* nll = (RooNLLVar*)mc->GetPdf()->createNLL(*data);
      minimize(nll, firstPOI);//RooFitResult* rest = mc->GetPdf()->fitTo(*data,Save(),Hesse(0),Minos(0),Strategy(strategy),PrintLevel(0));
      if (firstPOI->getVal() <= 0)
      {
	double minNll = nll->getVal();
	firstPOI->Print();
	cout << "mu error hi "<<firstPOI->getErrorHi()<<endl;
	double firstmuhat = firstPOI->getVal();
	double firsterror = firstPOI->getError();

	// set mu at low value, evaluate nll to see if low value looks trust worthy
	firstPOI->setVal(firstmuhat-firsterror);
	//RooAbsReal* nll = mc->GetPdf()->createNLL(*data);
	nll->setEvalErrorLoggingMode(RooAbsReal::CountErrors);
	cout << "Creating profile" << endl;
	//RooAbsReal* profile =   nll->createProfile(*mc->GetParametersOfInterest());
	double nllAtmuLo = nll->getVal()-minNll;//rest->minNll();
	cout << "nll = "<< nllAtmuLo<<endl;
	cout << "nll errors= "<< nll->numEvalErrors()<<endl;
	//  double pllAtmuLo = profile->getVal();
	//  cout << "pll = "<< pllAtmuLo<<endl;
	isatboundary=0;

	// set mu back to best fit
	firstPOI->setVal(firstmuhat);

	// if problems, set min to best fit
	if(nllAtmuLo>10 || nll->numEvalErrors()>0){
	  //firstPOI->setMin(firstmuhat);
	  isatboundary=1;
	}

	// get minos errors either way
      }
      mc->GetPdf()->fitTo(*data,Hesse(0),Minos(*mc->GetParametersOfInterest()),Strategy(strategy),PrintLevel(0));
    }
  }

  firstPOI->Print();
  double muhat = firstPOI->getVal();
  if (!errIsComputed)
  {
    errup = firstPOI->getErrorHi();
    errdown = firstPOI->getErrorLo();
  }
  else
  {
    if (errup != errup) errup = 0;
    if (errdown != errdown) errdown = 0;
    cout << "errup = " << errup << ", errdown = " << errdown << endl;
    firstPOI->Print();
  }
  stringstream outFileName;
  outFileName << "root-files/" << outFolder;
  system(("mkdir -vp " + outFileName.str()).c_str());
  outFileName << "/" << mass << ".root";

  TFile* file = new TFile(outFileName.str().c_str(),"recreate");
  TH1D* h_muhat = new TH1D("muhat","muhat",3,0,3);
  h_muhat->SetBinContent(1, muhat);
  h_muhat->SetBinContent(2, errup);
  h_muhat->SetBinContent(3, errdown);
  file->Write();
  file->Close();
  cout << "muhat: " << muhat << " +" << errup << " -" << -errdown << endl;
  timer.Stop();
  timer.Print();
  return muhat;
}


void minimize(RooNLLVar* nll, RooRealVar* firstPOI)
{
  int nrItr = 0;
  bool retry = false;
  do
  {
    retry = false;
    
    //minimize(nll);//mc->GetPdf()->fitTo(*data,Save(),Strategy(strategy),PrintLevel(0));

    int strat = ROOT::Math::MinimizerOptions::DefaultStrategy();
    RooMinimizer minim(*nll);
    minim.setStrategy(strat);
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    minim.setPrintLevel(ROOT::Math::MinimizerOptions::DefaultPrintLevel());

    if (status != 0 && status != 1 && strat < 2)
    {
      strat++;
      cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << endl;
      minim.setStrategy(strat);
      status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    }

    if (status != 0 && status != 1 && strat < 2)
    {
      strat++;
      cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << endl;
      minim.setStrategy(strat);
      status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    }


    if (firstPOI)
    {
      if (fabs(firstPOI->getVal() - firstPOI->getMax()) < 0.01)
      {
	firstPOI->setMax(2*firstPOI->getMax());
	retry = true;
      }
    
    
      if (fabs(firstPOI->getVal() - firstPOI->getMin()) < 0.000001)
      {
	if (firstPOI->getMin() != 0)
	{
	  firstPOI->setMin(firstPOI->getMin()*2);
	  retry = true;
	}
      }
    }
    
    nrItr++;
    if (nrItr > 10)
    {
      cout << "Infinite loop detected in minimization. Exiting.." << endl;
      return;
    }
  } while (retry);
}


void getError(int direction, RooNLLVar* nll, RooRealVar* firstPOI, double& err)
{
  int printLevel = ROOT::Math::MinimizerOptions::DefaultPrintLevel();
  ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(-1);

  firstPOI->setConstant(1);
//see if its at a wall
  
  double mu_val = firstPOI->getVal();

//   if (direction == -1)
//   {
//     if (firstPOI->getVal() - 0.02 < firstPOI->getMin())
//     {
//       err = 0;
//       return;
//     }

//     firstPOI->setVal(firstPOI->getVal()- 0.02);
//     minimize(nll);
//     if (nll->getVal() < -pow(10., 27.) || nll->getVal() != nll->getVal())
//     {
//       err = 0;
//       return;
//     }
//   }

  firstPOI->setVal(mu_val);

  double last_good_mu = firstPOI->getVal();
  double target = 0.5;
  int itr = 0;
  double nll_val = nll->getVal();
  double thisNll_val = nll->getVal();
  double step_size = max(0.2, firstPOI->getError()*0.5);
  double step_size_min = 0.002;
  double oldNll_val = nll->getVal();
  bool doBreak = false;
  while (!doBreak)
  {
    oldNll_val = thisNll_val;
    double old_mu = firstPOI->getVal();
    firstPOI->setVal(old_mu + step_size*direction);
    double this_mu = firstPOI->getVal();
    minimize(nll, firstPOI);
    thisNll_val = nll->getVal();
    if (thisNll_val != thisNll_val || thisNll_val > 10e30 || thisNll_val < -10e30 || fabs(thisNll_val - nll_val) > 1) // back off until we get back to normal
    {
      step_size *= 0.2;
      if (step_size < step_size_min) 
      {
	thisNll_val = nll_val + target+0.000000001;
	cout << "break-1" << endl;
	break;
      }
      firstPOI->setVal(last_good_mu);
      cout << "step size is now " << step_size << endl;
    }
    else
    {
      last_good_mu = old_mu;
      if (fabs(thisNll_val - nll_val) <= target)
      {
	//step_size = step_size/(thisNll_val - oldNll_val)*target;
	//cout << "Using new step size = " << step_size << endl;
      }
    }

   

    itr++;

    stringstream nll_val_str;
    if (thisNll_val != thisNll_val)
    {
      nll_val_str << "nan";
    }
    else
    {
      nll_val_str << thisNll_val;
    }

    stringstream dpll_val_str;
    if (thisNll_val != thisNll_val)
    {
      dpll_val_str << "nan";
    }
    else
    {
      dpll_val_str << nll_val - thisNll_val;
    }


    if (fabs(firstPOI->getVal() - firstPOI->getMin()) < 0.000001)
    {
      if (firstPOI->getMin() == 0)
      {
	cout << "break0" << endl;
	doBreak = true;
      }
      else
      {
	firstPOI->setMin(firstPOI->getMin()*2);
      }
    }

    cout << "Iterator " << itr << " gives nll val = " << nll_val_str.str() << " for mu = " << this_mu 
	 << ", delta(pll) = " << dpll_val_str.str() << ", delta(mu) = " << this_mu - mu_val << endl;

    if (itr > 300)
    {
      cout << "ERROR::Infinite loop detected in getError. Human intervention required." << endl;
      doBreak = true;
    }


    if (fabs(firstPOI->getVal() - firstPOI->getMin()) < 0.00001 && fabs(firstPOI->getMin()) < 0.00001) 
    {
      cout << "break1" << endl;
      doBreak = true;
    }
    cout << "thisNll_val = " << thisNll_val << endl;
    bool isErr = thisNll_val != thisNll_val || thisNll_val > 10e30 || thisNll_val < -10e30;
    cout << "isErr ? " << isErr << endl;
    if ((fabs(thisNll_val - nll_val) >= target && fabs(thisNll_val - nll_val) < 1) && !isErr)
    {
      cout << "break2" << endl;
      doBreak = true;
    }
  }
  if (direction == 1)
  {

  }
//interpolate
  err = interp(target+nll_val, thisNll_val, oldNll_val, firstPOI->getVal(), firstPOI->getVal() - direction*step_size) - mu_val;//
  
  ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(printLevel);
}

//previously used for number counting
double interp(double x, double x1, double x2, double y1, double y2)
{
  double m = (y2-y1)/(x2-x1);
  double b = y2 - m*x2;
  return m*x+b;
}



/*
double getError2(int direction, RooNLLVar* nll, RooRealVar* firstPOI, double& err)
{

//some variables used in avoiding infinite loop bouncing back and forth between two mu vals
  double p_nm2 = 0;
  bool even = true;
  double factor = 1.;
  //cout << "pdf val = " << pdf2->getVal() << endl;
  //cout << endl;

  bool tryZero = false;
  int maxItr = 50; // maximum iterations
  int itr = 0;
  double pmu=1;
  double precision = 0.005; // precision requested in limit; defines loop cutoff
  static double epsilon = 0.05; // small value used to compute numerical derivative
  static int nrInf = 0; // inf counter
  double testStat_val_dummy;
  cout << "Epsilon:   " << epsilon << endl;
  cout << "Precision: " << precision << endl;
  cout << "ncpScale: " << ncpScale << endl;
  do
  {
//control if it's an even or odd iteration
    if (even) even = false;
    else even = true;
    if (even) p_nm2 = pmu;

    cout << endl << endl;
    cout << "---------------------------" << endl;
    cout << "Starting iteration: " << itr++ << endl;
    //cout << "mu_val: " << mu_val << endl;

//compute pmu/(1-pb) at mu
//   mu->setVal(mu_val);
//   cout << "pdf val = " << pdf2->getVal() << endl;
    minimize(nll);//pmu=calcPprime_mu(mu_val, mu, pdf, data, asimovData, conditionalSnapshot, nominalSnapshot, ws, testStat_val, &nuis, ncpScale, mc);
    nll_val = nll->getVal();
    cout << "nll: " << nll_val << endl;





//try again if there's some numerical error
//     if (testStat_val < 0)
//     {
//       mu_val *=1.1;
//       itr++;
//       continue;
//     }
//     cout << endl;
    cout << "Computing at mu*(1+epsilon)" << endl;

    //double mu_val2 = mu_val*(1 + epsilon);
    firstPOI->setVal(mu_val*(1+epsilon));
    minimize(nll); 
//compute p-val ratio at mu*(1+epsilon) and find difference with previous p-val ratio
    double delta_nll = nll->getVal() - nll_val;//double delta_pmu=calcPprime_mu(mu_val2, mu, pdf, data, asimovData, conditionalSnapshot, nominalSnapshot, ws, testStat_val_dummy, &nuis, ncpScale, mc) - pmu;
    cout << "delta_pmu: " << delta_pmu << endl;

//again, try again if there's a numerical error
//     if (testStat_val_dummy < 0)
//     {
//       mu_val *= 1 + epsilon*2;
//       itr++;
//       continue;
//     }

    //mc->GetGlobalObservables()->Print("v");

//if the N-2'th iteration == N'th iteration, it will end up in an infinite loop.
//try putting an adaptive scale factor in front of the next order correction factor to aid in convergence.
    if (p_nm2 > 0 && fabs(p_nm2 - pmu)/p_nm2 < precision) 
    {
      cout << "Changing factor: " << factor*0.8 << endl;
      factor *= 0.8;

      if (factor < pow(0.8, 5.0)) factor = 1;
    }

//get the next order correction and apply it to mu
    double diff = factor*(pmu-target_p)/(delta_pmu/(mu_val2 - mu_val));
    if (diff > mu_val) 
    {
      if (mode == 0)
      {
	diff = 0.5*mu_val;
      }
      else //clsb
      {
	diff = mu_val;
      }
    }
    cout << "diff: " << diff << endl;
    mu_val -= diff;

//check for inf. retry if it is, maximum of 3 times. adjust epsilon a little higher,
//sometimes it's too small
    if (mu_val > 10e9 || mu_val < -10e9)
    {
      nrInf++;
      if (nrInf > 3) return mu_val;
      epsilon *= 2;
      return asymptoticCLs(mc, data, asimovData, ws, conditionalSnapshot, nominalSnapshot, testStat_val, CL, initialGuess, ncpScale);
    }
    if ((testStat_val_dummy > 10e9 || testStat_val_dummy < -10e9) || 
	(testStat_val > 10e9 || testStat_val < -10e9))
    {
      nrInf++;
      if (nrInf > 3) return mu_val;
      epsilon *= 2;
      return asymptoticCLs(mc, data, asimovData, ws, conditionalSnapshot, nominalSnapshot, testStat_val, CL, initialGuess, ncpScale);
    }

//precision cutoff
    if (fabs(diff/mu_val)<precision || mu_val != mu_val || mu_val > 10e9 || mu_val < -10e9 || (mode != 1 && mu_val <= 0)) break;
  } while (true && itr < maxItr);

}
*/
