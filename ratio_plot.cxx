#include <iostream>
#include <fstream>
#include <vector>
#include "TGraph.h"
using namespace std;

int ratio_plot(){
	ifstream inFile;
	inFile.open("VBFWWNWA_36_sig.txt");
	if(!inFile) cerr << "Unable to open file" << endl;

	ifstream inFile2;
	inFile2.open("VBFWWNWA_100_sig.txt");
	if(!inFile) cerr << "Unable to open file" << endl;
	
	string line;
	const vector <Double_t> mass_1;
	const vector <Double_t> sig_1;
	const vector <Double_t> mass_2;
	const vector <Double_t> sig_2;
	const vector <Double_t> sig_ratio;

	while(getline(inFile, line))
	{
		istringstream ss(line);
		int mass;
		double sig;
		ss >> mass >> sig;
		cout << "mass is: " << mass << endl;
		cout << "sig is: " << sig << endl;
		mass_1.push_back(mass);
		sig_1.push_back(sig);
	}

	string line2;
	while(getline(inFile2, line2))
	{
		istringstream ss(line2);
		int mass;
		double sig;
		ss >> mass >> sig;
		cout << "mass2 is: " << mass << endl;
		cout << "sig2 is: " << sig << endl;
		mass_2.push_back(mass);
		sig_2.push_back(sig);
	}

	if(mass_1.size() != mass_2.size()){
		cerr << "Mass sizes are not the same!!!" << endl;
		exit(1);

	}

	cout << "mass_1 size: " << mass_1.size() << endl;
	for(int i; i< mass_1.size(); i++){
		cout << "mass_1: " << mass_1.at(i) << " mass_2:" << mass_2.at(i) << endl;
		if(mass_1.at(i) == mass_2.at(i)){
			sig_ratio.push_back(sig_2.at(i)/sig_1.at(i));		
			cout << "sig1: " << sig_1.at(i) << " sig2: " << sig_2.at(i) << endl;
		}

	}

	TCanvas* c1 = new TCanvas("c1","c1",1024,768);
	TGraph* graph = new TGraph(mass_1.size(), &mass_1[0], &sig_ratio[0]);
	graph->SetTitle("Significance Ratio vs. MC mass; MC Mass [GeV]; 100fb sigma / 36fb sigma");
	graph->Draw("LA*");
	c1->SaveAs("ratio.pdf");
}
