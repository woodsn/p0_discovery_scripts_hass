#!/bin/sh

#for v in v5_HVT_WW
#for v in v5_HVT_WW_lvqq
for v in v5_HVT_WW_qqqq
do
    for im in 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000
    do
	bsub -q 2nd -o ${PWD}/${v}_${im}.out -J ${v}_${im} -u oabouzei "${PWD}/BSubSlave ${im} ${v} ${PWD}"
    done
done
