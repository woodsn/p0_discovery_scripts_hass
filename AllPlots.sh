## p0
root -l -b -q 'script/drawPlot_p0.C("v11_WW", 0)' 
root -l -b -q 'script/drawPlot_p0.C("v11_ZZ", 0)' 
root -l -b -q 'script/drawPlot_p0.C("v11_VV", 0)' 
#root -l -b -q 'script/drawPlot_p0.C("v11_WW", 0,0,"v9_WW")' 
#root -l -b -q 'script/drawPlot_p0.C("v11_ZZ", 0,0,"v9_ZZ")' 
#root -l -b -q 'script/drawPlot_p0.C("v11_VV", 0,0,"v9_VV")' 
### WW
root -l -b -q 'script/drawPlot_limit.C("v11_WW_Abs", 0,1,"v11_WWqqqq_Abs:qqqq","v11_lvqq_Abs:lvqq","","",1)'
root -l -b -q 'script/drawPlot_limit.C("v11_WW_Abs", 0,0)'
#root -l -b -q 'script/drawPlot_limit.C("v11_WW_Abs", 0,1,"v9_WW_Abs:Buggy_Syst")'
### ZZ
root -l -b -q 'script/drawPlot_limit.C("v11_ZZ_Abs", 0,1,"v11_ZZqqqq_Abs:qqqq","","v11_llqq_Abs:llqq","v11_vvqq_Abs:vvqq",1)'
root -l -b -q 'script/drawPlot_limit.C("v11_ZZ_Abs", 0,0)'
#root -l -b -q 'script/drawPlot_limit.C("v11_ZZ_Abs", 0,1,"v9_ZZ_Abs:Buggy_Syst")'
### VV
root -l -b -q 'script/drawPlot_limit.C("v11_VV_VV_Abs", 0,1,"v11_VVqqqq_VV_Abs:qqqq", "v11_lvqq_VV_Abs:lvqq", "v11_llqq_VV_Abs:llqq", "v11_vvqq_VV_Abs:vvqq",1)'
root -l -b -q 'script/drawPlot_limit.C("v11_VV_VV_Abs", 0,1,"v11_ZZ_VV_Abs:ZZ","","","v11_WW_VV_Abs:WW",1)'
root -l -b -q 'script/drawPlot_limit.C("v11_VV_VV_Abs", 0,0)'
#root -l -b -q 'script/drawPlot_limit.C("v11_VV_VV_Abs", 0,1,"v9_VV_VV_Abs:Buggy_Syst")'
### Other
#root -l -b -q 'script/drawPlot_limit.C("v11_VVqqqq_VV_Abs", 0, 1, "v9_VVqqqq_VV_Abs:Buggy_Syst")'
#root -l -b -q 'script/drawPlot_limit.C("v11_WWqqqq_Abs", 0, 1, "v9_WWqqqq_Abs:Buggy_Syst")'
#root -l -b -q 'script/drawPlot_limit.C("v11_ZZqqqq_Abs", 0, 1, "v9_ZZqqqq_Abs:Buggy_Syst")'
#root -l -b -q 'script/drawPlot_limit.C("v11_llqq_Abs", 0, 1, "v9_llqq_Abs:Buggy_Syst")'
#root -l -b -q 'script/drawPlot_limit.C("v11_lvqq_Abs", 0, 1, "v9_lvqq_Abs:Buggy_Syst")'
#root -l -b -q 'script/drawPlot_limit.C("v11_vvqq_Abs", 0, 1, "v9_vvqq_Abs:Buggy_Syst")'
