#!/bin/sh


v="v23"


for iv in "LWA5" "LWA10" #"LWA15"
do
    version=${v}${iv}
    for im in 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000
    do
	./bin/runAsymptoticsCLs ${im} ${version}
    done
done

