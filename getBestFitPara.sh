#!/bin/sh


for v in v81CPSGGF v81CPSVBF #v81NWAGGF v81NWAVBF 
#for v in  v81NWAGGF v81NWAVBF 
do
    for ichan in 503
    do
	for im in 300 400 500 600 650 700 750 800 900 1000
	do
	    if [[ $v == *NWA* ]]; then
		im=${im}NWA
	    fi

            logname=log/bestFit_${v}_${ichan}_${im}.log
            rm ${logname}; root -l -b -q  script/runHatMu.cxx\(${ichan},\"${im}\",\"${v}\"\) | tee ${logname}
	done
    done
done