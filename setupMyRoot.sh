
#source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
#lsetup  "root 5.34.21-x86_64-slc6-gcc47-opt" # HAZ

### HSG7 Root
#source /afs/cern.ch/sw/lcg/external/gcc/4.8.4/x86_64-slc6-gcc48-opt/setup.sh
#cd /afs/cern.ch/atlas/project/HSG7/root/root_v5-34-32/x86_64-slc6-gcc48 # HAZ
#source bin/thisroot.sh
#cd -

### AFS Root
source /afs/cern.ch/sw/lcg/external/gcc/4.7.2/x86_64-slc6-gcc47-opt/setup.sh
cd /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.21/x86_64-slc6-gcc47-opt/root
source bin/thisroot.sh
cd -

#source /afs/cern.ch/sw/lcg/external/gcc/4.6.3/x86_64-slc6-gcc46-opt/setup.sh 
#source /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.17/x86_64-slc6-gcc46-opt/root/bin/thisroot.sh

#export PATH=/afs/cern.ch/sw/lcg/external/Python/2.7.2/x86_64-slc5-gcc43-opt/bin:$PATH
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/cern.ch/sw/lcg/external/Python/2.7.2/x86_64-slc5-gcc43-opt/lib

export LD_LIBRARY_PATH=${PWD}/:${PWD}/script:${PWD}/macros:${PWD}/tmp:${PWD}/lib:${PWD}/bin:${LD_LIBRARY_PATH}
export PATH=.:${PWD}/bin:${PWD}/script:${PWD}/macros:${PWD}/lib:${ROOTSYS}/include:${ROOTSYS}/lib:${ROOTSYS}/bin:${PATH}
