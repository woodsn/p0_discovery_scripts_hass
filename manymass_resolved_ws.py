import condor, time, os, commands,sys
timestr = time.strftime("%m%d%Y_%H%M")

signal = "HVTWW"
mass = ["700","800"]
exe = 'root -l -b -q script/runP0.cxx'
files = "dummy.txt"
#for s in signal:
#	if s=="VBF-HVTWW" or s=="VBF-HVTWZ":
for m in mass:
	argtemplate = ' %s%s%s%s ' ( "(\"" , m , "\",\"" , signal , "\")" )
        dirname = 'ws_' +s+'_'+m+'_'+'_'+timestr
	print argtemplate
	condor.run(exe,argtemplate,files, dirname, 1)
