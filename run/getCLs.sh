#!/bin/sh


v="v35"


for isys in 1
  do
  for ichan in 501 502 503
  do
      #for im in 560 580 600 650 700 750 800 850 900 950 1000
      #for im in 600 700 800 900 1000 # NWA
      #for im in 400 420 440 460 480 500 520 540 560 580 600 650 700 750 800 850 900 950 1000
      #for im in 300 320 340 360 380 400 420 440 460 480 500 520 540 560 580 600 650 700 750 800 850 900 950 1000 #CPS
      for im in 360 380 400 420 440 460 480 500 520 540 560 580 600 650 700 750 800 850 900 950 1000 #CPS
      do
	  if [ ${isys} -eq 1 ] ; then
	      logfile=log/${v}_cls_${ichan}_${im}.log
	  else
	      logfile=log/${v}_cls_${ichan}_${im}_nosys.log
	  fi
	  
	  rm -f ${logfile} ; bsub_lxplus 8nh ${logfile} "./bin/runAsymptoticsCLs ${ichan} ${isys} ${im} ${v}; root -l -b -q \"script/runP0.cxx(${ichan}, ${isys}, ${im} )\"  ; root -l -b -q \"script/runHatMu.cxx(${ichan}, ${isys}, ${im} )\"  "
      done
  done
done
