#!/bin/sh
for isys in 0 1
#for isys in  1
  do
  for ichan in 1 2 
    do
    for im in 300 305 310 315 320 325 330 335 340 345 350 360 370 380 390 400 420 440 460 480 500 520 540 560 580 600 
      do
      if [ ${isys} -eq 1 ] ; then
	  ./bin/makeworkspace ${im} ${isys} ${ichan} | tee log/makeworkspace_${im}_${ichan}.log
      else
	  ./bin/makeworkspace ${im} ${isys} ${ichan} | tee log_nosys/makeworkspace_${im}_${ichan}.log
      fi
    done
  done
done
