#!/bin/sh
for isys in 0 1
do
for ichan in 1 2 3
do
for im in 300 305 310 315 320 325 330 335 340 345 350 360 370 380 390 400 420 440 460 480 500 520 540 560 580 600 
do
if [ ${isys} -eq 1 ] ; then
 ./bin/readworkspace ${im} ${isys} ${ichan} | tee log/readworkspace_${im}_${ichan}.log
else
 ./bin/readworkspace ${im} ${isys} ${ichan} | tee log_nosys/readworkspace_${im}_${ichan}.log
fi
done
done
done
