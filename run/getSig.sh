#!/bin/sh
for isys in 0 1
  do
  for ichan in 501 502 503
    do
    for im in 300 305 310 315 320 325 330 335 340 345 350 360 370 380 390 400 420 440 460 480 500 520 540 560 580 600
    #for im in 300 305 310 
      do
      if [ ${isys} -eq 1 ] ; then
	  logfile=log/sig_${ichan}_${im}.log
      else
	  logfile=log/sig_${ichan}_${im}_nosys.log
      fi
      rm -f ${logfile} ; bsub_lxplus 8nh ${logfile} root -l -b -q \"script/runP0.cxx\(${ichan}, ${isys}, ${im} \)\"
    done
  done
done
