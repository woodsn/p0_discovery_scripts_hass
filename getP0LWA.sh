#!/bin/sh



v=v24LWA15


for im in 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000
do
    logname=log/p0_${v}_${im}.log
    rm ${logname};root -l -b -q  script/runP0.cxx\(\"${im}\",\"${v}\"\) | tee ${logname}
done
