#!/bin/sh

v=v1_lvqq

for im in 800 900 1000 1400 1800 2200 2600 3000
do
    ./bin/runAsymptoticsCLs ${im} ${v}
done
