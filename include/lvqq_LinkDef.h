#include "Background.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

//#pragma link C++ class RooLnuqqSignal+;
//#pragma link C++ class RooLnuqqHighmass+;
#pragma link C++ class Background+;
#endif
