#!/bin/sh

for v in v5_HVT_WW
do
    for im in 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000
    do
	bsub -q 8nh -o ${PWD}/p0_${v}_${im}.out -J ${v}_${im}_p0 -u oabouzei "${PWD}/BSubSlave_p0 ${im} ${v} ${PWD}"
    done
done
