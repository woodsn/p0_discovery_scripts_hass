// C++
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>

// Root
#include "TFile.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TCanvas.h"
#include "TList.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TMarker.h"
// RooFit
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooHist.h"
#include "RooSimultaneous.h"
#include "RooCategory.h"
#include "RooFitResult.h"
#include "RooAbsData.h"
#include "RooRealSumPdf.h"
#include "Roo1DTable.h"
#include "RooConstVar.h"
#include "RooProduct.h"
#include "RooRandom.h"
#include "TStopwatch.h"
#include "RooNLLVar.h"
#include "RooMsgService.h"
// RooStat
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileInspector.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/SamplingDistribution.h"
#include "RooStats/SamplingDistPlot.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/RooStatsUtils.h"
#include "RooStats/MinNLLTestStat.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

  



void makePostFitPdfPlot(TString ofilename = "../v23/hwwlvqq_combined_1400NWA_model.root" )
{
  bool bkgOnly = 0;  
  double mu = 0.; if (bkgOnly) mu =0.;

  TFile *outputfile = new TFile("FitCrossChecks.root","RECREATE");

  
  bool drawPlots = 1 ;
  TString OutputDir = "EPS";

  TString wname="combined";
  TString dataName = "obsData";

  TFile *file = TFile::Open(ofilename);

  cout << "reading " << wname << " from " << ofilename << endl;
  RooWorkspace* wspace = (RooWorkspace*) file->Get(wname);  
  //wspace->Print();
  RooAbsData* data = (RooAbsData*)wspace->data(dataName);
  ModelConfig* mc = (ModelConfig*) wspace->obj("ModelConfig");

  RooAbsPdf* pdf=mc->GetPdf();
  const RooArgSet* Obs = mc->GetObservables() ;
  const RooArgSet* globalObs = mc->GetGlobalObservables(); 

  RooArgSet* constraints = pdf->getAllConstraints(*data->get(),*pdf->getParameters(data));
  
  cout << "ParametersOfInterest:: " << mc->GetParametersOfInterest()->getSize() << endl;
  RooRealVar* firstPOI = (RooRealVar*) mc->GetParametersOfInterest()->first();
  cout << "firstPOI:: " ; firstPOI->Print();

  

  const RooArgSet* nuisPars = mc->GetNuisanceParameters(); 
  RooSimultaneous *simPdf = (RooSimultaneous*)(mc->GetPdf());
  
  // Fit 
  if (bkgOnly){ firstPOI->setConstant(1);}
  firstPOI->setConstant(0); 
  wspace->saveSnapshot("snapshot_paramsVals_PreFit",*nuisPars);

  ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);
  //RooFitResult    *fitresGlobal  = simPdf->fitTo( *data , Save() , Minos(UseMinosError) );
  RooFitResult *fitresGlobal  = simPdf->fitTo(*data,Save(true),Extended(true),Hesse(false),Minos(false),Constrain(*nuisPars),PrintLevel(-1), RooFit::Offset(1)); 
  fitresGlobal->Print();
  wspace->saveSnapshot("snapshot_paramsVals_GlobalFit",*nuisPars);
  double muhat = firstPOI->getVal();
  firstPOI->setConstant(kFALSE);
  
  
  ///----------------------------------------------------------------------------
  // Plotting the distributions for each subchannel
  RooCategory* channelCat = (RooCategory*) (&simPdf->indexCat());
  TIterator *iter = channelCat->typeIterator() ;
  RooCatType *tt  = NULL;
  vector<double> Chi2Channel;Chi2Channel.clear();
  vector<TString> NameChannel;NameChannel.clear();
  TString dirName(OutputDir+"/PlotsAfterGlobalFit");
  if(drawPlots) { system(TString("mkdir -vp "+dirName)); }
  
  while((tt=(RooCatType*) iter->Next()) ){
    
    RooAbsPdf  *pdftmp  = simPdf->getPdf( tt->GetName() );
    RooAbsData *datatmp = data->reduce(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),tt->GetName()));
    RooArgSet  *obstmp  = pdftmp->getObservables( *mc->GetObservables() ) ;
    RooRealVar *obs     = ((RooRealVar*) obstmp->first());
    
    // Bin Width
    RooRealVar* binWidth = ((RooRealVar*) pdftmp->getVariables()->find(Form("binWidth_obs_x_%s_0",tt->GetName()))) ;
    if(!binWidth) { cout << "No bin width " << tt->GetName() << endl; }
    cout << "    Bin Width : " << binWidth->getVal() << endl;
    
    // Load the value from the global fit
    if(!wspace->loadSnapshot("snapshot_paramsVals_GlobalFit")) {
      cout << "Cannot load " <<  "snapshot_paramsVals_GlobalFit" << endl;
      exit(-1);
    }
    if   (bkgOnly) firstPOI->setVal(0.);
    
    
    TString modelName(tt->GetName());
    modelName.Append("_model");
    RooRealSumPdf *pdfmodel = (RooRealSumPdf*) (pdftmp->getComponents())->find(modelName);
    RooArgList funcList =  pdfmodel->funcList();
    RooProduct* comp = 0;
    RooLinkedListIter funcIter = funcList.iterator() ;
    cout << "Post Fit " << endl;
    while( (comp = (RooProduct*) funcIter.Next()) ) {
      cout << "\t DEBUGNEW " << comp->GetName() << "===>  " << (comp->createIntegral(*obs))->getVal() * binWidth->getVal() << endl;
    }
    
    TString cname = "can_DistriAfterFit_"+  modelName  +"_GlobalFit_mu";
    cname += mu;
    

    TCanvas* c2 = new TCanvas( cname );
    
    //obs->Print();
    RooPlot* frame = obs->frame();
    
    TString FrameName = "Plot_DistriGlobal";
    frame->SetName( FrameName.Data()  );
    frame->SetYTitle("EVENTS");


    float postFitIntegral = pdftmp->expectedEvents(*obs);
    datatmp->plotOn(frame,MarkerSize(1),Name("Data"),DataError(RooAbsData::Poisson));
    pdftmp->plotOn(frame,FillColor(kOrange),LineWidth(2),LineColor(kBlue),VisualizeError(*fitresGlobal,1),
		   Normalization(postFitIntegral,RooAbsReal::NumEvent),Name("FitError_NotAppears"));
    pdftmp->plotOn(frame,LineWidth(2),Normalization(postFitIntegral,RooAbsReal::NumEvent),Name("CentralFit_NotAppears"));
    double chi2 = frame->chiSquare();
    Chi2Channel.push_back( chi2 );
    NameChannel.push_back( modelName );
    
    // miniloop over componant to make a bkg stack
    TString modelName1(tt->GetName());
    modelName1.Append("_model");
    RooRealSumPdf *pdfmodel1 = (RooRealSumPdf*) (pdftmp->getComponents())->find(modelName1);
    RooArgList funcList1 =  pdfmodel1->funcList();
    RooLinkedListIter funcIter1 = funcList1.iterator() ;
    RooProduct* comp1 = 0;
    if (bkgOnly) firstPOI->setVal(0.);
    else         firstPOI->setVal(muhat);
    
    int ibkg=0;
    int icolor=0;
    TString previous="";
    while( (comp1 = (RooProduct*) funcIter1.Next()) ) {
      ibkg++;
      


      int color=kGray;
      
      TString compname(comp1->GetName());
      compname.ReplaceAll("L_x_","");
      compname.ReplaceAll(tt->GetName(),""); 
      compname.ReplaceAll("_overallSyst_x_StatUncert","");
      compname.ReplaceAll("_overallSyst_x_HistSyst","");
      compname.ReplaceAll("_overallSyst_x_Exp","");
      compname.ReplaceAll("_","");
      
      icolor++;
      color=getColor(icolor);

      // Get the signal x 1 in white (for further purposes)
      if (bkgOnly) firstPOI->setVal(0.);
      else         firstPOI->setVal(muhat);
      double Ntemp=(comp1->createIntegral(*obs))->getVal() * binWidth->getVal();
      pdfmodel1->plotOn(frame,LineWidth(0),Components(*comp1),LineColor(0), LineStyle(3), Normalization(Ntemp,RooAbsReal::NumEvent),Name("NoStacked_"+compname));
      if (bkgOnly) firstPOI->setVal(0.);
      else         firstPOI->setVal(muhat);
      
      // Stack bkg
      Ntemp=(comp1->createIntegral(*obs))->getVal() * binWidth->getVal();
      if (ibkg==0) pdfmodel1->plotOn(frame,LineWidth(2),Components(*comp1),LineColor(color), Normalization(Ntemp,RooAbsReal::NumEvent),Name("Stacked_"+compname));
      else         pdfmodel1->plotOn(frame,LineWidth(2),Components(*comp1),LineColor(color), Normalization(Ntemp,RooAbsReal::NumEvent),Name("Stacked_"+compname),AddTo(previous));
      previous="Stacked_"+compname;
      

      // save Histograms for post-fit
      wspace->loadSnapshot("snapshot_paramsVals_GlobalFit") ; firstPOI->setVal(1.);
      Ntemp=(comp1->createIntegral(*obs))->getVal() * binWidth->getVal();
      TString histName = "postfit_Hist_" + compname ;
      TH1* hpostfit = comp1->createHistogram(histName,*obs);
      hpostfit->SetLineColor(kRed);
      hpostfit->Scale(Ntemp/hpostfit->Integral());
      outputfile->Add(hpostfit);

      // save Histograms for pre-fit
      wspace->loadSnapshot("snapshot_paramsVals_PreFit") ; firstPOI->setVal(1.);
      Ntemp=(comp1->createIntegral(*obs))->getVal() * binWidth->getVal();
      histName = "prefit_Hist_" + compname ;
      TH1* hprefit = comp1->createHistogram(histName,*obs);
      hprefit->SetLineColor(kBlue);
      hprefit->Scale(Ntemp/hprefit->Integral());
      outputfile->Add(hprefit);

      TString ratioName = hpostfit->GetName();
      hpostfit->Sumw2();
      hprefit->Sumw2();
      
      ratioName.ReplaceAll("postfit", "PosOverPre") ; 
      //TString ratioName = "PosOverPre_Hist_" + compname ;
      TH1 *PosOverPre = (TH1*)hpostfit->Clone(ratioName);PosOverPre->SetName(ratioName);
      PosOverPre->Divide(hprefit);
      outputfile->Add(PosOverPre);

      wspace->loadSnapshot("snapshot_paramsVals_GlobalFit") ;
    }

    
    
    if (!bkgOnly) firstPOI->setVal(muhat);
    else          firstPOI->setVal(0.);
    pdftmp->plotOn(frame,FillColor(kOrange),LineWidth(2),LineColor(kBlue),VisualizeError(*fitresGlobal,1),
                     Normalization(postFitIntegral,RooAbsReal::NumEvent),Name("FitError_AfterFit"+modelName));
    pdftmp->plotOn(frame,LineWidth(2),Normalization(postFitIntegral,RooAbsReal::NumEvent),Name("FitCentral2_NotAppears"));
    datatmp->plotOn(frame,MarkerSize(1),Name("Dat_NotAppears"),DataError(RooAbsData::Poisson));


    TObject *objtmp = frame->findObject("FitError_AfterFit"+modelName);
    TGraph *tmp = (TGraph*) objtmp;
    outputfile->cd();  tmp->Write();


    // Putting nuisance parameter at the central value and draw the nominal distri
    if(!wspace->loadSnapshot("snapshot_paramsVals_PreFit")) {
      cout << "Cannot load " <<  "snapshot_paramsVals_PreFit" << endl;
      exit(-1);
    }
      
    SetPOI(wspace, mc, 0.0);
    
    TString muValueBeforeFitLegend = Form("Before fit (#mu=%2.2f)",firstPOI->getVal());
    funcIter = funcList.iterator() ;
    cout << "Pre Fit " << endl;
    while( (comp = (RooProduct*) funcIter.Next()) ) {
      cout << "\t" << comp->GetName() << "\t" << (comp->createIntegral(*obs))->getVal() * binWidth->getVal() << endl;
    }
    float preFitIntegral = pdftmp->expectedEvents(*obs);
    pdftmp->plotOn(frame,LineWidth(2),Name("BeforeFit"),LineStyle(kDashed),Normalization(preFitIntegral,RooAbsReal::NumEvent));
    c2->cd(); 
    frame->Draw();
    c2->cd();


    /* no working yet
    
    // -1 sigma
    wspace->loadSnapshot("snapshot_paramsVals_GlobalFit") ;
    SetAllNuisanceParaToSigma(wspace, mc, -1);
    SetPOI(wspace, mc, 0.);
    TString histName = "m1sigma_" + modelName;
    TH1* hm1sigma = pdftmp->createHistogram(histName,*obs);
    //cout << "DEBUGNEW " << pdftmp->expectedEvents(*obs) << ", " << hm1sigma->Integral()  << endl;
    
    hm1sigma->Scale(pdftmp->expectedEvents(*obs) / hm1sigma->Integral());     
    outputfile->Add(hm1sigma);
    
    //+1 sigma
    wspace->loadSnapshot("snapshot_paramsVals_GlobalFit") ;
    SetAllNuisanceParaToSigma(wspace, mc, 1);
    SetPOI(wspace, mc, 0.);
    histName = "p1sigma_" + modelName;
    TH1* hp1sigma = pdftmp->createHistogram(histName,*obs);
    hp1sigma->Scale(pdftmp->expectedEvents(*obs) / hp1sigma ->Integral() );     
    outputfile->Add(hp1sigma);
    

    // Nominal
    wspace->loadSnapshot("snapshot_paramsVals_GlobalFit") ;
    SetPOI(wspace, mc,0.);
    histName = "Center_" + modelName;
    TH1* hnominal = pdftmp->createHistogram(histName,*obs);
    hnominal->Scale(pdftmp->expectedEvents(*obs) / hnominal->Integral() );
    outputfile->Add(hnominal);
    */
    
 
    c2->cd();
    frame->Draw();
    TLatex text;  
    text.SetTextAlign(31);
    TString WritDownMuValue;
    if(!bkgOnly) WritDownMuValue = "#mu_{best} = ";
    else        WritDownMuValue = "#mu_{fixed} = ";
    WritDownMuValue += Form("%2.2f",firstPOI->getVal());
    
    text.DrawLatex( 0.73,0.81, WritDownMuValue );
    TString ts_chi2 = Form("#chi^{2}=%1.1f", chi2 );
    text.DrawLatex( 0.22, 0.83, ts_chi2 );
    
    TLegend *leg = new TLegend(0.54,0.40,0.77,0.76);
    leg->SetBorderSize(0);
    leg->SetFillColor(0);
    leg->SetTextFont(62);
    leg->SetTextSize(0.050);

    for (int i=0; i<frame->numItems(); i++) {
      TString obj_name=frame->nameOf(i); 
      TObject *objtmp = frame->findObject(obj_name.Data());
      
      cout << "DEBUG2, "  << obj_name << ", "<< objtmp->ClassName() << endl;  


      if (obj_name=="" || obj_name.Contains("NotAppears") || obj_name.Contains("NoStacked") ) continue;
      TObject *obj = frame->findObject(obj_name.Data());
      
      if (obj_name.Contains("Data")) { leg->AddEntry( obj , "Data" , "p"); continue; }
      TString legname;
      if (!bkgOnly) legname = "After fit (#mu[best] S+B)";
      else                 legname = "After fit (#mu[fixed] S+B)";
      
      if (obj_name.Contains("AfterFit") ){ leg->AddEntry( obj , legname , "lf"); continue; }
      legname = muValueBeforeFitLegend;
      
      if ( obj_name.Contains("BeforeFit") ){leg->AddEntry( obj ,legname , "l"); continue;}
      
      obj_name.ReplaceAll("Stacked_","");
      leg->AddEntry(obj,obj_name,"l");
    }

    leg->Draw();
    // Save the plots
    //MainDir->cd();
    //c2->Write();

    if(drawPlots) {
      c2->Print(dirName+"/"+c2->GetName()+".pdf");
    }
    c2->Close();
    gROOT->cd();


    cout << "Number of items in frame " << frame->numItems() << endl;
    frame->Clear();
    cout << "Number of items in frame " << frame->numItems() << endl;
    delete frame;
    frame = 0;

    

    } // loop over channels

   


  outputfile->Write();
  outputfile->Close();

}


void myText(Double_t x,Double_t y,Color_t color, const char *text, Double_t size) {

  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(size);
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}
void setStuffR(RooPlot* rp){
  float size = 0.09;
  rp->GetYaxis()->SetLabelSize(size);
  rp->GetYaxis()->SetTitleSize(size);
  rp->GetYaxis()->SetTitleOffset(0.7);
  rp->GetYaxis()->CenterTitle();
  rp->GetXaxis()->SetLabelSize(size);
  rp->GetXaxis()->SetTitleSize(size);
  rp->GetXaxis()->SetTitleOffset(1.2);
  
}
 
void SetAllStatErrorToSigma(RooWorkspace* w,  ModelConfig* mc, double Nsigma){

    TIterator* it = mc->GetNuisanceParameters()->createIterator();
    RooRealVar* var = NULL;
    while( (var = (RooRealVar*) it->Next()) ){
      string varname = var->GetName();
      if ( varname.find("gamma_stat")!=string::npos ){
        RooAbsReal* nom_gamma = (RooConstVar*) w->obj( ("nom_" + varname).c_str() );
        double nom_gamma_val = nom_gamma->getVal();
        double sigma = 1/TMath::Sqrt( nom_gamma_val );
        var->setVal(1 + Nsigma*sigma);
      }
    }

    return;
}
 void SetAllNuisanceParaToSigma(RooWorkspace* w,  ModelConfig* mc,double Nsigma){

    TIterator* it = mc->GetNuisanceParameters()->createIterator();
    RooRealVar* var = NULL;
    while( (var = (RooRealVar*) it->Next()) ){
      string varname = var->GetName();
      if ( varname.find("gamma_stat")!=string::npos ) continue;
      if(strcmp(var->GetName(),"Lumi")==0){
        var->setVal(w->var("nominalLumi")->getVal()*(1+Nsigma*LumiRelError));
      } else{
        var->setVal(Nsigma);
      }
    }

    return;
  }

void SetPOI(RooWorkspace* w,  ModelConfig* mc,double mu){
  RooRealVar * firstPOI = dynamic_cast<RooRealVar*>(mc->GetParametersOfInterest()->first());
  firstPOI->setVal(mu);
  return  ;
}



int getColor(int icolor)
{
  int color=kGray;
  if(icolor==1) { color = kOrange - 3; }
  else if(icolor==2) { color = kCyan + 1; }
  else if(icolor==3) { color = kGreen - 9; }
  else if(icolor==4) { color = kAzure - 9; }
  else if(icolor==5) { color = kOrange + 10;}
  else if(icolor==6) { color = kGreen - 6; }
  else if(icolor==7) { color = kAzure - 4; }
  else if(icolor==8) { color = kOrange + 6; }
  else if(icolor==9) { color = kGreen + 1; }
  else if(icolor==10) { color = kAzure + 2; }
  else if(icolor==11) { color = kOrange; }
  else if(icolor==12) { color = kGreen + 3; }
  else if(icolor==13) { color = kAzure - 4; }
  else if(icolor==14) { color = kOrange; }
  else if(icolor==15) { color = kGreen + 1; }
  else if(icolor==16) { color = kOrange - 7; }
  else if(icolor==17) { color = kPink + 1; }
  else   color=icolor;

  return color;
}

void SetAllNuisanceParaToSigma(RooWorkspace* w,  ModelConfig* mc, double Nsigma){
  
  TIterator* it = mc->GetNuisanceParameters()->createIterator();
  RooRealVar* var = NULL;
  while( (var = (RooRealVar*) it->Next()) ){
    string varname =  var->GetName();
    
    if ( varname.find("gamma_stat")!=string::npos ) continue;

    double value = var->getVal();
    double error = var->getError();
    var->setVal(value + error*Nsigma);

    
    //if(strcmp(var->GetName(),"Lumi")==0){
    //  var->setVal(w->var("nominalLumi")->getVal()*(1+Nsigma*LumiRelError));
    //  } else{
    //  var->setVal(Nsigma);
    //}
  }

  return;
}

